<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Auth
$route['logout'] = 'user/logout';
$route['login'] = 'auth/login';
$route['login_check'] = 'auth/loginCheck';
$route['forgot_password'] = 'auth/forgotPassword';
$route['send_link'] = 'auth/sendLink';
$route['reset_password/(:any)'] = 'auth/resetPassword/$1';
$route['reset'] = 'auth/reset';
$route['verification/(:any)'] = 'auth/emailVerification/$1';

//User Management
$route['user'] = 'user/index';
$route['user/table'] = 'user/userTable';    
$route['user/create'] = 'user/create';
$route['user/store'] = 'user/store';
$route['user/edit/(:num)'] = 'user/edit/$1';
$route['user/update/(:num)'] = 'user/update/$1';
$route['user/destroy/(:num)'] = 'user/destroy/$1';

//Change Password
$route['change_password'] = 'user/changePassword';
$route['change'] = 'user/change';

//Category
$route['category'] = 'category/index';
$route['category/table'] = 'category/categoryTable';    
$route['category/create'] = 'category/create';
$route['category/store'] = 'category/store';
$route['category/edit/(:num)'] = 'category/edit/$1';
$route['category/update/(:num)'] = 'category/update/$1';
$route['category/destroy/(:num)'] = 'category/destroy/$1';

//Management Product Type
$route['product_type'] = 'product_type/index';
$route['product_type/table'] = 'product_type/productTypeTable';    
$route['product_type/create'] = 'product_type/create';
$route['product_type/store'] = 'product_type/store';
$route['product_type/edit/(:num)'] = 'product_type/edit/$1';
$route['product_type/update/(:num)'] = 'product_type/update/$1';
$route['product_type/destroy/(:num)'] = 'product_type/destroy/$1';

//Management Product
$route['product'] = 'product/index';
$route['product/table'] = 'product/productTable';    
$route['product/create'] = 'product/create';
$route['product/store'] = 'product/store';
$route['product/edit/(:num)'] = 'product/edit/$1';
$route['product/update/(:num)'] = 'product/update/$1';
$route['product/destroy/(:num)'] = 'product/destroy/$1';

//Table Number (Manajemen Nomor Meja)
$route['table'] = 'table_number/index';
$route['table/table'] = 'table_number/tableNumberTable';    
$route['table/create'] = 'table_number/create';
$route['table/store'] = 'table_number/store';
$route['table/edit/(:num)'] = 'table_number/edit/$1';
$route['table/update/(:num)'] = 'table_number/update/$1';
$route['table/destroy/(:num)'] = 'table_number/destroy/$1';

//Order (Pesanan Live)
$route['order'] = 'order/index';
$route['order/search_menu_category/(:num)'] = 'order/searchMenu/$1';
$route['order/new_order'] = 'order/newOrder';
$route['order/store_order'] = 'order/storeOrder';
$route['order/cancel_order'] = 'order/cancelOrder';
$route['order/change_status_order'] = 'order/changeStatusOrder';
$route['order/add_menu/(:num)/(:num)'] = 'order/addMenu/$1/$2';
$route['order/remove_menu/(:num)/(:num)'] = 'order/removeMenu/$1/$2';
$route['order/detail_order/(:num)'] = 'order/detailOrder/$1';

//Order List (Pesanan Berlangsung)
$route['order_list'] = 'order_list/index';
$route['order_list/table'] = 'order_list/orderListTable';
$route['order_list/edit/(:num)'] = 'order_list/edit/$1';
$route['order_list/destroy/(:num)'] = 'order_list/destroy/$1';
$route['order_list/change_status/(:num)'] = 'order_list/changeStatus/$1';
$route['order_list/update_status/(:num)/(:any)'] = 'order_list/updateStatus/$1/$2';

//Payment
$route['payment'] = 'payment/index';
$route['payment/detail_order/(:num)'] = 'payment/detailOrder/$1';
$route['payment/detail_order_id/(:num)'] = 'payment/detailOrderId/$1';
$route['payment/pay'] = 'payment/pay';

//Order Complete (Pesanan Selesai)
$route['order_complete'] = 'order_list/complete';
$route['order_complete/table'] = 'order_list/completeTable';
$route['order_complete/payment_revision/(:num)'] = 'order_list/paymentRevision/$1';
$route['order_complete/detail_order/(:num)'] = 'order_list/detailOrder/$1';

//Revision (Pesanan Revisi)
$route['revision'] = 'order_list/revision';
$route['revision/table'] = 'order_list/revisionTable';
$route['revision/do_revision/(:num)'] = 'order_list/doRevision/$1';
$route['revision/add_menu_revision/(:num)/(:num)'] = 'order_list/addMenu/$1/$2';
$route['revision/remove_menu_revision/(:num)/(:num)'] = 'order_list/removeMenu/$1/$2';
$route['revision/add_money_back/(:num)'] = 'order_list/addMoneyBack/$1';
$route['revision/add_payment/(:num)'] = 'order_list/addPayment/$1';
$route['revision/finish/(:num)'] = 'order_list/finish/$1';

//Report
$route['statistic/(:num)/(:num)'] = 'report/statistic/$1/$2';
$route['statistic/detail_order/(:num)'] = 'report/detailOrder/$1';
$route['statistic/print_year/(:num)'] = 'report/printYear/$1';
$route['statistic/print_month/(:num)/(:num)'] = 'report/printMonth/$1/$2';






