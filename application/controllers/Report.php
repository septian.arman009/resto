<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('query_model', 'qm');
        $this->load->model('basic_model', 'bm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function statistic($month, $year)
    {
        $data['view'] = 'admin/report/statistic';
        $data['scripts'] = 'admin/report/_scripts';
        $data['_modal'] = 'admin/template/_modal';
        $data['title'] = 'Statistik Penjualan';
        $data['menu'] = 'report';
        $data['sub'] = 'statistic';
        $data['year'] = $year;
        $data['month'] = $month;
        $income = [];
        $orders = $this->db->select('month(created_at) as month, sum(total_order) as income')
                                ->where('year(created_at)', $year)
                                ->group_by('month(created_at)')
                                ->get('customer_orders')->result();

        for ($i = 0; $i <= 11; $i++) {
            if (!empty($orders[$i])) {
                $income[] = [
                    'month' => toIndoMonth($orders[$i]->month),
                    'income' => $orders[$i]->income,
                ];

            } else {
                $income[] = [
                    'month' => toIndoMonth($i+1),
                    'income' => 0,
                ];
            }

        }

        $data['income'] = $income;
        $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $monthOrders = $this->db->select('day(created_at) as day, sum(total_order) as income_day')
                                    ->where('month(created_at)', $month)
                                    ->where('year(created_at)', $year)
                                    ->group_by('day(created_at)')
                                    ->get('customer_orders')->result();
        $income_day = [];
        if ($monthOrders) {

			foreach ($monthOrders as $order) {
				$mOrder[$order->day] = [
					'day' => "Tgl : ".$order->day,
					'income_day' => $order->income_day,
				];

				$day[] = $order->day;
			}

            for ($i = 1; $i <= $days; $i++) {
				if (in_array($i, $day)) {
					$income_day[] = [
						'day' => $mOrder[$i]['day'],
						'income_day' => $mOrder[$i]['income_day'],
					];
				} else {
					$income_day[] = [
						'day' => "Tgl : $i",
						'income_day' => 0,
					];
				}
            }

        } else {
            $income_day[] = [
                'day' => "No Data",
                'income_day' => 0,
            ];

		}

        $data['income_day'] = $income_day;
        $data['details'] = $this->db->select("order_id, customer_name, total_order, 
                                                DATE_FORMAT(created_at, '%d/%m/%Y %H:%i:%s') AS created_at")
                                        ->where('month(created_at)', $month)
                                        ->where('year(created_at)', $year)
                                        ->get('customer_orders')
                                        ->result();
        $this->load->view('admin/template/app', $data);
    }

    public function detailOrder($orderId)
    {       
        $data['order'] = $this->qm->getDetailOrderList($orderId);
        $data['details'] = $this->qm->getDetailOrderId($orderId);
        $this->load->view('admin/report/_detail_order', $data);
    }

    public function printYear($year)
    {
        $data['year'] = $year;
        $orders = $this->db->select('month(created_at) as month, sum(total_order) as income')
                                ->where('year(created_at)', $year)
                                ->group_by('month(created_at)')
                                ->get('customer_orders')->result();
        $income = [];
        for ($i = 0; $i <= 11; $i++) {
            if (!empty($orders[$i])) {
                $income[] = [
                    'month' => toIndoMonth($orders[$i]->month),
                    'income' => $orders[$i]->income,
                ];

            } else {
                $income[] = [
                    'month' => toIndoMonth($i+1),
                    'income' => 0,
                ];
            }

        }
        $data['orders'] = $income;
        $this->load->library('fpdf');
        $this->load->view('admin/report/print_year', $data);
    }

    public function printMonth($year, $month)
    {
        $data['month'] = $month;
        $data['orders'] = $this->db->select("customer_name, total_order, DATE_FORMAT(created_at, '%d/%m/%Y %H:%i:%s') AS created_at")
                                        ->where('month(created_at)', $month)
                                        ->get('customer_orders')
                                        ->result();
        $this->load->library('fpdf');
        $this->load->view('admin/report/print_month', $data);
    }

}

