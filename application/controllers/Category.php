<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['view'] = 'admin/category/index';
        $data['scripts'] = 'admin/category/_scripts';
        $data['title'] = 'Kategori';
        $data['menu'] = 'master';
        $data['sub'] = 'category';
        $this->load->view('admin/template/app', $data);
    }

    public function categoryTable()
    {   
        $query = "SELECT a.category_id, a.category_name, b.product_type_name, 
                    DATE_FORMAT(a.created_at, '%d/%m/%Y %H:%i:%s') AS created_at,
                    DATE_FORMAT(a.updated_at, '%d/%m/%Y %H:%i:%s') AS updated_at
                    FROM categories As a
                    LEFT JOIN product_types AS b ON a.product_type_id = b.product_type_id";

        $datatables = [
            'query' => $query,
            'column' => [
                'category_id',
                'category_name',
                'product_type_name',
                'created_at',
                'updated_at',
            ],
            'searchOperator' => 'HAVING',
            'edit_url' => base_url('category/edit/'),
            'delete_url' => base_url('category/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus kategori : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'category_name', //Pilih salah satu dari table $column
            ],
            'action' => 'basic_action', //Level action
        ];

        showQueryTable($datatables);
    }

    
    public function create()
    {
        $data['view'] = 'admin/category/create';
        $data['scripts'] = 'admin/category/_scripts';
        $data['title'] = 'Kategori - Tambah Data';
        $data['menu'] = 'master';
        $data['sub'] = 'category';
        $data['productTypes'] = $this->db->get('product_types')->result();
        $this->load->view('admin/template/app', $data);
    }

    public function store()
    {
        if($this->vm->categoryStore()){
            $data = [
                'category_name' => ucwords($this->input->post('category_name')),
                'product_type_id' => $this->input->post('product_type_id'),
                'created_at' => date('Y-m-d H:i:d'),
                'updated_at' => date('Y-m-d H:i:d')
            ];
            $this->db->insert('categories', $data);
            response(['message' => 'Berhasil menambah kategori']);
        }else{
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function edit($id)
    {
        $category = $this->db->get_where('categories', ['category_id' => $id])->result_array(); 

        $data['view'] = 'admin/category/edit';
        $data['scripts'] = 'admin/category/_scripts';
        $data['title'] = 'Kategori - Edit Data';
        $data['menu'] = 'master';
        $data['sub'] = 'category';
        $data['category'] = $category[0];
        $data['typeId'] = $category[0]['product_type_id'];
        $data['productTypes'] = $this->db->get('product_types')->result();
        $this->load->view('admin/template/app', $data);
    }

    public function update($id)
    {
        if($this->vm->categoryUpdate($id)){
            $data = [
                'category_name' => ucwords($this->input->post('category_name')),
                'product_type_id' => $this->input->post('product_type_id'),
                'updated_at' => date('Y-m-d H:i:d')
            ];
            $this->db->where('category_id', $id)->update('categories', $data);
            $this->session->set_flashdata('notify', [
                'message' => "Berhasil mengubah kategori",
                'level' => "success",
            ]);
            response();
        }else{
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function destroy($id)
    {
        $this->db->delete('categories', ['category_id' => $id]);
        $this->db->delete('products', ['category_id' => $id]);
        response(['id' => $id]);
    }

}