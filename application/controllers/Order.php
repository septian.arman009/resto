<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('query_model', 'qm');
        $this->load->model('basic_model', 'bm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['view'] = 'admin/order/index';
        $data['scripts'] = 'admin/order/_scripts';
        $data['_modal'] = 'admin/template/_modal';
        $data['title'] = 'Pemesanan';
        $data['menu'] = 'order';
        $data['sub'] = '';
        $data['types'] = $this->db->get('product_types')->result();
        $data['categories'] = $this->qm->getCategoriesQty();
        //1 = progress, 2 = booked, 3 = payed/finish
        $data['order'] = $this->qm->getOrderStatus(1);

        $this->load->view('admin/template/app', $data);
    }

    public function searchMenu($categoryId)
    {
        $html = '';
        $products = $this->db->get_where('products', ['category_id' => $categoryId])->result();
        if($products){
            foreach ($products as $product) {
                if(!is_null($product->photo)){
                    $photo_url = base_url('assets/admin/products/'.$product->photo);
                }else{
                    $photo_url = base_url('assets/admin/images/no_image.jpg');
                }
    
                $html .= '<li>
                            <a onclick="addMenu('.$product->product_id.')">
                                <span class="image">
                                    <img style="width: 120px; height:100px; border: 1px solid #ccc; border-radius: 10px" src="'.$photo_url.'"
                                        alt="img">
                                </span>
                                <span>
                                    <span style="font-size: 18px">'.$product->product_name.'</span>
                                    <span style="font-size: 18px" class="time">'.toRp($product->price).'</span>
                                </span>
                                <span class="message">
                                    '.$product->description.'
                                </span>
                            </a>
                        </li>';
            }
    
            echo $html;
        }else{
            echo '<li>
                    <a>
                        <span class="image">
                            <img style="width: 100px" src="'.base_url('assets/admin/images/no_image.jpg').'"
                                alt="img">
                        </span>
                        <span>
                            <span style="font-size: 18px">Belum ada produk terdaftar dikategori ini...</span>
                            <span style="font-size: 18px" class="time"></span>
                        </span>
                        <span class="message">
                            
                        </span>
                    </a>
                </li>';
        }
        
    }

    public function newOrder()
    {
        $data['tables'] = $this->qm->getTablesNotInOrders();
        $this->load->view('admin/order/_new_order', $data);
    }

    public function storeOrder()
    {
        if($this->vm->orderStore()){
            $data = [
                'table_id' => $this->input->post('table_id'),
                'customer_name' => $this->input->post('customer_name'),
                'total_order' => 0,
                'status' => 1,
                'order_status' => 'Pesanan diterima',
                'created_at' => date('Y-m-d H:i:s')
            ];

            $this->db->insert('customer_orders', $data);
            $this->session->set_flashdata('notify', [
                'message' => "Berhasil membuat pesanan baru",
                'level' => "success",
            ]);
            response();
        }else{
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function cancelOrder()
    {
        $orderId = $this->bm->getField('customer_orders', 'order_id', 'status', 1)->order_id;
        $this->db->delete('customer_orders', ['status'=> 1]);
        $this->db->delete('detail_orders', ['order_id'=> $orderId]);
        $this->session->set_flashdata('notify', [
            'message' => "Berhasil membatalkan pesanan",
            'level' => "success",
        ]);
        response();
    }

    public function addMenu($orderId, $productId)
    {
        $checkProduct = $this->db->get_where('detail_orders', ['order_id' => $orderId, 'product_id' => $productId])->row();
        if(!$checkProduct){
            $price = $this->bm->getField('products', 'price', 'product_id', $productId)->price;
            $data = [
                'order_id' => $orderId,
                'product_id' => $productId,
                'qty' => 1,
                'order_price' => $price,
                'sub_total' => $price * 1
            ];
            $this->db->insert('detail_orders', $data);
            $updateTotalOrder = $this->qm->updateTotalOrder("+ $price", $orderId);
        }else{
            $qty = $checkProduct->qty + 1;
            $data = [
                'qty' => $qty,
                'sub_total' => $checkProduct->sub_total + $checkProduct->order_price 
            ];
            $this->db->where(['order_id' => $orderId, 'product_id' => $productId])->update('detail_orders', $data);
            $updateTotalOrder = $this->qm->updateTotalOrder("+ $checkProduct->order_price", $orderId);
        }
        
        response([
            'rule' => 'order',
            'total_order' => "Total Pesanan : ".torp($updateTotalOrder)
        ]);
    }

    public function removeMenu($orderId, $productId)
    {
        $checkProduct = $this->db->get_where('detail_orders', ['order_id' => $orderId, 'product_id' => $productId])->row();
       
        $qty = $checkProduct->qty - 1;
        if($qty > 0){
            $data = [
                'qty' => $qty,
                'sub_total' => $checkProduct->sub_total - $checkProduct->order_price 
            ];
            $this->db->where(['order_id' => $orderId, 'product_id' => $productId])->update('detail_orders', $data);
        }else{
            $this->db->delete('detail_orders' , ['order_id' => $orderId, 'product_id' => $productId]);
        }

        $updateTotalOrder = $this->qm->updateTotalOrder("- $checkProduct->order_price", $orderId);
        
        response([
            'rule' => 'order',
            'total_order' => "Total Pesanan : ".torp($updateTotalOrder)
        ]);
    }

    public function detailOrder($orderId)
    {
        $data['details'] = $this->qm->getDetailOrderId($orderId);
        $this->load->view('admin/order/_detail_order', $data);
    }

    public function changeStatusOrder()
    {
        $this->db->where('status', 1)->update('customer_orders', ['status' => 2]);
        $this->session->set_flashdata('notify', [
            'message' => "Pesanan baru telah diterima",
            'level' => "success",
        ]);
        response();
    }

    public function kwitansi($orderId)
    {
        $data['title'] = 'Cetak Kwitansi';
        $data['order'] = $this->qm->getDetailOrderList($orderId);
        $data['details'] = $this->qm->getDetailOrderId($orderId);
        $this->load->view('admin/order/_kwitansi', $data);
    }

}