<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order_list extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('query_model', 'qm');
        $this->load->model('basic_model', 'bm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['view'] = 'admin/order_list/index';
        $data['scripts'] = 'admin/order/_scripts';
        $data['_modal'] = 'admin/template/_modal';
        $data['title'] = 'Pesanan Berlangsung';
        $data['menu'] = 'transaction';
        $data['sub'] = 'order_list';
        $data['table_url'] = base_url('order_list/table');
        $this->load->view('admin/template/app', $data);
    }

    public function orderListTable()
    {   
        $query = "SELECT a.order_id, a.customer_name ,b.table_name, a.order_status,
                        DATE_FORMAT(a.created_at, '%d/%m/%Y %H:%i:%s') AS created_at, 
                        CONCAT('Rp. ', fRupiah(a.total_order)) AS total_order
                            FROM customer_orders AS a 
                            LEFT JOIN table_numbers AS b 
                            USING(table_id)
                            WHERE a.status = 2";

        $datatables = [
            'query' => $query,
            'column' => [
                'order_id',
                'customer_name',
                'table_name',
                'created_at',
                'total_order',
                'order_status'
            ],
            'searchOperator' => 'HAVING',
            'edit_url' => base_url('order_list/edit/'),
            'delete_url' => base_url('order_list/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus pesanan customer : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'customer_name', //Pilih salah satu dari table $column
            ],
            'action' => 'orderlist_action', //Level action
        ];

        showQueryTable($datatables);
    }

    public function destroy($id)
    {
        $this->db->delete('customer_orders', ['order_id' => $id]);
        $this->db->delete('detail_orders', ['order_id' => $id]);
        response(['id' => $id]);
    }

    public function edit($id)
    {
        $data['view'] = 'admin/order_list/edit';
        $data['scripts'] = 'admin/order/_scripts';
        $data['title'] = 'Pesanan - Edit Data';
        $data['menu'] = 'transaction';
        $data['sub'] = 'order_list';
        $data['types'] = $this->db->get('product_types')->result();
        $data['categories'] = $this->qm->getCategoriesQty();
        //1 = progress, 2 = booked, 3 = payed/finish
        $data['order'] = $this->qm->getDetailOrderList($id);
        $this->load->view('admin/template/app', $data);
    }

    //Order Finish
    public function complete()
    {
        $data['view'] = 'admin/order_complete/index';
        $data['scripts'] = 'admin/order/_scripts';
        $data['title'] = 'Pesanan Selesai';
        $data['menu'] = 'transaction';
        $data['sub'] = 'order_complete';
        $data['table_url'] = base_url('order_complete/table');
        $this->load->view('admin/template/app', $data);
    }

    public function completeTable()
    {   
        $query = "SELECT a.order_id, a.customer_name ,b.table_name, 
                        DATE_FORMAT(a.created_at, '%d/%m/%Y %H:%i:%s') AS created_at, 
                        CONCAT('Rp. ', fRupiah(a.total_order)) AS total_order
                            FROM customer_orders AS a 
                            LEFT JOIN table_numbers AS b 
                            USING(table_id)
                            WHERE a.status = 3";

        $datatables = [
            'query' => $query,
            'column' => [
                'order_id',
                'customer_name',
                'table_name',
                'created_at',
                'total_order',
            ],
            'searchOperator' => 'HAVING',
            'edit_url' => base_url('order_complete/payment_revision/'),
            'delete_url' => base_url('order_list/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin akan merevisi pembayaran customer : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'customer_name', //Pilih salah satu dari table $column
            ],
            'action' => 'order_complete', //Level action
        ];

        showQueryTable($datatables);
    }

    public function paymentRevision($orderId)
    {
        $this->db->where('order_id', $orderId)->update('customer_orders', ['status' => 4]);
        response(['id' => $orderId]);
    }

    //Order Finish
    public function revision()
    {
        $data['view'] = 'admin/order_complete/revision';
        $data['scripts'] = 'admin/order/_scripts';
        $data['title'] = 'Revisi Pesanan';
        $data['menu'] = 'transaction';
        $data['sub'] = 'revision';
        $data['table_url'] = base_url('revision/table');
        $this->load->view('admin/template/app', $data);
    }

    public function revisionTable()
    {   
        $query = "SELECT a.order_id, a.customer_name ,b.table_name, 
                        DATE_FORMAT(a.created_at, '%d/%m/%Y %H:%i:%s') AS created_at, 
                        CONCAT('Rp. ', fRupiah(a.total_order)) AS total_order
                            FROM customer_orders AS a 
                            LEFT JOIN table_numbers AS b 
                            USING(table_id)
                            WHERE a.status = 4";

        $datatables = [
            'query' => $query,
            'column' => [
                'order_id',
                'customer_name',
                'table_name',
                'created_at',
                'total_order',
            ],
            'searchOperator' => 'HAVING',
            'edit_url' => base_url('revision/do_revision/'),
            'delete_url' => null,
            'confirm' => [
                'message' => "", //c_column (Nama, Id Dll)+c
                'c_column' => "", //Pilih salah satu dari table $column
            ],
            'action' => 'order_revision', //Level action
        ];

        showQueryTable($datatables);
    }

    public function doRevision($id)
    {
        $data['view'] = 'admin/order_complete/do_revision';
        $data['scripts'] = 'admin/order_complete/_scripts_revision';
        $data['title'] = 'Revisi Pesanan - Revisi Data';
        $data['menu'] = 'transaction';
        $data['sub'] = 'revision';
        $data['types'] = $this->db->get('product_types')->result();
        $data['categories'] = $this->qm->getCategoriesQty();
        //1 = progress, 2 = booked, 3 = payed/finish
        $data['order'] = $this->qm->getDetailOrderList($id);
        $this->load->view('admin/template/app', $data);
    }

    public function addMenu($orderId, $productId)
    {
        $checkProduct = $this->db->get_where('detail_orders', ['order_id' => $orderId, 'product_id' => $productId])->row();
        if(!$checkProduct){
            $price = $this->bm->getField('products', 'price', 'product_id', $productId)->price;
            $data = [
                'order_id' => $orderId,
                'product_id' => $productId,
                'qty' => 1,
                'order_price' => $price,
                'sub_total' => $price * 1
            ];
            $this->db->insert('detail_orders', $data);
            $updateTotalOrder = $this->qm->updateTotalOrderRevision("+ $price", $orderId);
        }else{
            $qty = $checkProduct->qty + 1;
            $data = [
                'qty' => $qty,
                'sub_total' => $checkProduct->sub_total + $checkProduct->order_price
            ];
            $this->db->where(['order_id' => $orderId, 'product_id' => $productId])->update('detail_orders', $data);
            $updateTotalOrder = $this->qm->updateTotalOrderRevision("+ $checkProduct->order_price", $orderId);
        }
        $total_order = $updateTotalOrder->total_order;
        $extra = $updateTotalOrder->extra;
        response([
            'rule' => 'revision',
            'total_order' => toRp($total_order),
            'extra' => toRp($extra),
            'extra_money' => $extra
        ]);
    }

    public function removeMenu($orderId, $productId)
    {
        $checkProduct = $this->db->get_where('detail_orders', ['order_id' => $orderId, 'product_id' => $productId])->row();
       
        $qty = $checkProduct->qty - 1;

        if($qty > 0){
            $data = [
                'qty' => $qty,
                'sub_total' => $checkProduct->sub_total - $checkProduct->order_price 
            ];
            $this->db->where(['order_id' => $orderId, 'product_id' => $productId])->update('detail_orders', $data);
        }else{
            $this->db->delete('detail_orders' , ['order_id' => $orderId, 'product_id' => $productId]);
        }

        $updateTotalOrder = $this->qm->updateTotalOrderRevision("- $checkProduct->order_price", $orderId);
        
        $total_order = $updateTotalOrder->total_order;
        $extra = $updateTotalOrder->extra;

        response([
            'rule' => 'revision',
            'total_order' => toRp($total_order),
            'extra' => toRp($extra),
            'extra_money' => $extra
        ]);
    }

    public function addMoneyBack($orderId)
    {
        $order = $this->db->get_where('customer_orders', ['order_id' => $orderId])->row();

        $total_order = $order->total_order;
        $pay = $order->pay;
        $moneyBack = $order->money_back;
        $extra = abs($order->extra);

        $data = [
            'total_order' => $total_order,
            'pay' => $pay,
            'money_back' => $moneyBack + $extra,
            'money_back_revision' => $extra,
            'extra' => 0,
            'revision' => "Kekurangan kembalian pelanggan ".toRp($order->extra)
        ];
        $this->db->where('order_id', $orderId)->update('customer_orders', $data);
        $this->session->set_flashdata('notify', [
            'message' => "Kekurangan pembayaran telah ditambahkan",
            'level' => "success",
        ]);
        $this->session->set_flashdata('notify', [
            'message' => "Kembalian telah ditambahkan",
            'level' => "warning",
        ]);
        response();
    }

    public function addPayment($orderId)
    {
        $amount = moneyClean($this->input->post('add_payment'));
        $order = $this->db->get_where('customer_orders', ['order_id' => $orderId])->row();
        if($amount > 0 && $amount >= $order->extra){
        
            $total_order = $order->total_order;
            $extra = $order->extra;

            if($extra <= $order->money_back){
                $pay = $order->pay;
                $moneyBack = $order->money_back - $extra;
                $moneyBackRevision = $amount - $extra;
            }else{
                $pay = $order->pay + ($amount - $order->money_back);
                $moneyBack = $pay - $total_order;
                $moneyBackRevision = $amount - $extra;

            }

            $data = [
                'total_order' => $total_order,
                'pay' => $pay,
                'money_back' => $moneyBack,
                'money_back_revision' => $moneyBackRevision,
                'extra' => 0,
                'revision' => "Kekurangan pembayaran pelanggan ".toRp($order->extra)
            ];
            $this->db->where('order_id', $orderId)->update('customer_orders', $data);
            $this->session->set_flashdata('notify', [
                'message' => "Kekurangan pembayaran telah ditambahkan",
                'level' => "success",
            ]);
            response();
        }else{
            $errors = ['add_payment' => "* Tambahan pembayaran harus lebih besar atau sama dengan Extra"];
            response(['errors' => $errors]);
        }
    }

    public function finish($orderId)
    {
        $this->db->where('order_id', $orderId)->update('customer_orders', ['status' => 3]);
        $this->session->set_flashdata('notify', [
            'message' => "Revisi selesai",
            'level' => "success",
        ]);
        response();
    }

    public function detailOrder($orderId)
    {
        $data['view'] = 'admin/order_complete/detail_order';
        $data['scripts'] = 'admin/order_complete/_scripts_revision';
        $data['title'] = 'Detail Pesanan';
        $data['menu'] = 'transaction';
        $data['sub'] = 'order_complete';
        $data['order'] = $this->qm->getDetailOrderList($orderId);
        $data['details'] = $this->qm->getDetailOrderId($orderId);
        $this->load->view('admin/template/app', $data);
    }

    public function changeStatus($orderId)
    {
        $data['order'] = $this->db->get_where('customer_orders', ['order_id' => $orderId])->row();
        $this->load->view('admin/order_list/change_status', $data);
    }

    public function updateStatus($orderId, $response)
    {
        $data = [
            'order_status' => $this->input->post('order_status'),
            'updated_at' => date('Y-m-d H:i:s')
        ];
        
        $this->db->where('order_id', $orderId)->update('customer_orders', $data);

        $this->session->set_flashdata('notify', [
            'message' => "Berhasil mengubah status pemesanan",
            'level' => "success",
        ]);
        
        if($response == 'edit') redirect('order_list/edit/'.$orderId); else redirect('order_list');
        
    }

}