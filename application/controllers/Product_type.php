<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_type extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['view'] = 'admin/product_type/index';
        $data['scripts'] = 'admin/product_type/_scripts';
        $data['title'] = 'Jenis Produk';
        $data['menu'] = 'master';
        $data['sub'] = 'product_type';
        $this->load->view('admin/template/app', $data);
    }

    public function productTypeTable()
    {
        $datatables = [
            'table' => 'product_types',
            'column' => [
                'product_type_id',
                'product_type_name',
                'DATE_FORMAT(created_at, "%d/%m/%Y %H:%i:%s")',
                'DATE_FORMAT(updated_at, "%d/%m/%Y %H:%i:%s")'
            ],
            'edit_url' => base_url('product_type/edit/'),
            'delete_url' => base_url('product_type/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus jenis produk : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'product_type_name', //Pilih salah satu dari table $column
            ],
            'action' => 'basic_action', //Level action
        ];

        showOneTable($datatables);
    }

    
    public function create()
    {
        $data['view'] = 'admin/product_type/create';
        $data['scripts'] = 'admin/product_type/_scripts';
        $data['title'] = 'Jenis Produk - Tambah Data';
        $data['menu'] = 'master';
        $data['sub'] = 'product_type';
        $this->load->view('admin/template/app', $data);
    }

    public function store()
    {
        if($this->vm->productTypeStore()){
            $data = [
                'product_type_name' => ucwords($this->input->post('product_type_name')),
                'created_at' => date('Y-m-d H:i:d'),
                'updated_at' => date('Y-m-d H:i:d')
            ];
            $this->db->insert('product_types', $data);
            response(['message' => 'Berhasil menambah jenis produk']);
        }else{
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function edit($id)
    {
        $product_type = $this->db->get_where('product_types', ['product_type_id' => $id])->result_array(); 

        $data['view'] = 'admin/product_type/edit';
        $data['scripts'] = 'admin/product_type/_scripts';
        $data['title'] = 'Jenis Produk - Edit Data';
        $data['menu'] = 'master';
        $data['sub'] = 'product_type';
        $data['product_type'] = $product_type[0];
        $this->load->view('admin/template/app', $data);
    }

    public function update($id)
    {
        if($this->vm->productTypeUpdate($id)){
            $data = [
                'product_type_name' => ucwords($this->input->post('product_type_name')),
                'updated_at' => date('Y-m-d H:i:d')
            ];
            $this->db->where('product_type_id', $id)->update('product_types', $data);
            $this->session->set_flashdata('notify', [
                'message' => "Berhasil mengubah jenis produk",
                'level' => "success",
            ]);
            response();
        }else{
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function destroy($id)
    {
        $this->db->delete('product_types', ['product_type_id' => $id]);
        $this->db->delete('categories', ['product_type_id' => $id]);
        response(['id' => $id]);
    }
}