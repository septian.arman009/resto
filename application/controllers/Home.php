<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->helper('backend_helper');
		auth();
	}

	public function index()
	{
		$data['view'] = 'admin/home/index';
		$data['scripts'] = 'admin/home/_scripts';
		$data['title'] = 'Beranda';
		$data['menu'] = 'home';
		$data['sub'] = '';

		$days = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
		
		$monthOrders = $this->db->select('day(created_at) as day, sum(total_order) as income_day')
									->where('month(created_at)', date('m'))
									->group_by('day(created_at)')
									->get('customer_orders')
									->result();
		
		$income_day = [];
        if ($monthOrders) {

			foreach ($monthOrders as $order) {
				$mOrder[$order->day] = [
					'day' => "Tgl : ".$order->day,
					'income_day' => $order->income_day,
				];

				$day[] = $order->day;
			}

            for ($i = 1; $i <= $days; $i++) {
				if (in_array($i, $day)) {
					$income_day[] = [
						'day' => $mOrder[$i]['day'],
						'income_day' => $mOrder[$i]['income_day'],
					];
				} else {
					$income_day[] = [
						'day' => "Tgl : $i",
						'income_day' => 0,
					];
				}
            }

        } else {
            $income_day[] = [
                'day' => "No Data",
                'income_day' => 0,
            ];

		}
		
        $data['income_day'] = $income_day;
		$data['total_customer_perday'] = $this->db->select('count(customer_name) as total_customer_perday')
											->from('customer_orders')
											->where('day(created_at)', date('d'))
											->get()
											->row()->total_customer_perday;

		$data['total_customer_permonth'] = $this->db->select('count(customer_name) as total_customer_permonth')
											->from('customer_orders')
											->where('month(created_at)', date('m'))
											->get()
											->row()->total_customer_permonth;

		$data['total_order_perday'] = $this->db->select('sum(total_order) as total_order_perday')
											->from('customer_orders')
											->where('day(created_at)', date('d'))
											->get()
											->row()->total_order_perday;

		$data['total_order_permonth'] = $this->db->select('sum(total_order) as total_order_permonth')
											->from('customer_orders')
											->where('month(created_at)', date('m'))
											->get()
											->row()->total_order_permonth;

		$data['products'] = $this->db->query("SELECT a.product_name, sum(b.qty) as product_sell, 
													(SELECT SUM(qty) FROM detail_orders) as total_sell, 
													((sum(b.qty) / (SELECT SUM(qty) 
												FROM detail_orders)) * 100) as percen 
												FROM products a, detail_orders b 
												WHERE a.product_id = b.product_id 
												GROUP BY b.product_id 
												ORDER BY percen DESC 
												LIMIT 10")->result();

		$this->load->view('admin/template/app', $data);
    }
    
}
