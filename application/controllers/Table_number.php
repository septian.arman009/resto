<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Table_number extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['view'] = 'admin/table/index';
        $data['scripts'] = 'admin/table/_scripts';
        $data['title'] = 'Nomor Meja';
        $data['menu'] = 'master';
        $data['sub'] = 'table';
        $this->load->view('admin/template/app', $data);
    }

    public function tableNumberTable()
    {
        $datatables = [
            'table' => 'table_numbers',
            'column' => [
                'table_id',
                'table_name',
                'DATE_FORMAT(created_at, "%d/%m/%Y %H:%i:%s")',
                'DATE_FORMAT(updated_at, "%d/%m/%Y %H:%i:%s")'
            ],
            'edit_url' => base_url('table/edit/'),
            'delete_url' => base_url('table/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus angkatan : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'table_name', //Pilih salah satu dari table $column
            ],
            'action' => 'basic_action', //Level action
        ];

        showOneTable($datatables);
    }

    public function create()
    {
        $data['view'] = 'admin/table/create';
        $data['scripts'] = 'admin/table/_scripts';
        $data['title'] = 'Nomor Meja - Tambah Data';
        $data['menu'] = 'master';
        $data['sub'] = 'table';

        $tableNumbers = $this->db->select(['table_name'])->get('table_numbers')->result();
        $tbNumber = array();
        foreach ($tableNumbers as $tNumber) {
            $splitName = explode(' ', $tNumber->table_name);
            $tbNumber[] = $splitName[1];
        }
        $data['tbNumber'] = $tbNumber;
        $this->load->view('admin/template/app', $data);
    }

    public function store()
    {
        $data = [
            'table_name' =>  'Meja '.$this->input->post('table_name'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $this->db->insert('table_numbers', $data);
        response(['message' => $this->input->post('table_name')]);
    }

    public function edit($id)
    {
        $tableNumber = $this->db->get_where('table_numbers', ['table_id' => $id])->result_array();

        $data['view'] = 'admin/table/edit';
        $data['scripts'] = 'admin/table/_scripts';
        $data['title'] = 'Nomor Meja - Edit Data';
        $data['menu'] = 'master';
        $data['sub'] = 'table';
        $splitSm = explode(' ', $tableNumber[0]['table_name']);
        $data['tbNumberPosition'] = $splitSm[1];
        $data['tableNumber'] = $tableNumber[0];
        $tableNumbers = $this->db->select(['table_name'])
        ->where('table_name !=', $tableNumber[0]['table_name'])->get('table_numbers')->result();

        $tbNumber = array();
        foreach ($tableNumbers as $tNumber) {
            $splitName = explode(' ', $tNumber->table_name);
            $tbNumber[] = $splitName[1];
        }
        $data['tbNumber'] = $tbNumber;
        $this->load->view('admin/template/app', $data);
    }

    public function update($id)
    {
        $this->db->where('table_id', $id)->update('table_numbers', [
            'table_name' =>  'Meja '.$this->input->post('table_name'),
            'updated_at' => date('Y-m-d H:i:s')   
        ]);
        $this->session->set_flashdata('notify', [
            'message' => "Berhasil mengubah angkatan",
            'level' => "success",
        ]);
        response();
    }

    public function destroy($id)
    {
        $this->db->delete('table_numbers', ['table_id' => $id]);
        response(['id' => $id]);
    }

}