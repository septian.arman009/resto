<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payment extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('query_model', 'qm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['view'] = 'admin/payment/index';
        $data['scripts'] = 'admin/payment/_scripts';
        $data['title'] = 'Pembayaran';
        $data['menu'] = 'payment';
        $data['sub'] = '';
        $data['tables'] = $this->qm->getTablePayment();
        $this->load->view('admin/template/app', $data);
    }

    public function detailOrder($tableId)
    {
        $data['order'] = $this->qm->getDetailTableId($tableId);
        $data['details'] = $this->qm->getDetailOrderId($data['order']->order_id);
        $this->load->view('admin/payment/_detail_order', $data);
    }

    public function detailOrderId($orderId)
    {
        $data['order'] = $this->qm->getDetailOrderId2($orderId);
        $data['details'] = $this->qm->getDetailOrderId3($orderId);
        $this->load->view('admin/payment/_detail_order', $data);
    }

    public function pay()
    {
        $pay = moneyClean($this->input->post('pay'));
        $totalOrder = $this->input->post('total_order');
        $orderId = $this->input->post('order_id');
        
        if($pay >= $totalOrder){
            $data = [
                'pay' => $pay,
                'money_back' => $pay - $totalOrder,
                'status' => 3
            ];
            $this->db->where('order_id', $orderId)->update('customer_orders', $data);
            response(['message' => $orderId]);
        }else{
            $errors = [
                'pay' => 'Pembayaran harus lebih besar atau sama dengan total tagihan'
            ];
            response(['errors' => $errors]);
        }
    }
}