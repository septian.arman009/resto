<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function logout()
    {
        $this->session->unset_userdata(SESSION_KEY);
        response();
    }

    public function index()
    {
        $data['view'] = 'admin/user/index';
        $data['scripts'] = 'admin/user/_scripts';
        $data['title'] = 'User';
        $data['menu'] = 'master';
        $data['sub'] = 'user';
        $this->load->view('admin/template/app', $data);
    }

    public function userTable()
    {
        $datatables = [
            'table' => 'users',
            'column' => [
                'user_id',
                'name',
                'email',
                'DATE_FORMAT(created_at, "%d/%m/%Y %H:%i:%s")',
                'DATE_FORMAT(updated_at, "%d/%m/%Y %H:%i:%s")'
            ],
            'edit_url' => base_url('user/edit/'),
            'delete_url' => base_url('user/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus user : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'name', //Pilih salah satu dari table $column
            ],
            'action' => 'basic_action', //Level action
        ];

        showOneTable($datatables);
    }

    public function create()
    {
        $data['view'] = 'admin/user/create';
        $data['scripts'] = 'admin/user/_scripts';
        $data['title'] = 'User - Tambah Data';
        $data['menu'] = 'master';
        $data['sub'] = 'user';
        $this->load->view('admin/template/app', $data);
    }

    public function store()
    {
        
        if ($_FILES['photo']['name'] != "") {
            $upload = $this->upload($_FILES);
        }

        $e_verification = $this->input->post('verification');

        $data = [
            'name' => ucwords($this->input->post('name')),
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password')),
            'photo' => (!empty($upload['file_name'])) ? $upload['file_name'] : null,
            'verified' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        if ($this->vm->userStore() && empty($upload['errors'])) {
            if(is_null($e_verification)){
                $user = $this->db->insert('users', $data);
                response(['message' => 'Berhasil menambah user']);
            }else{
                $token = generateVerificationToken();
                $email = $this->input->post('email');
                $message = "Link Verifikasi : ".base_url('verification/'.$token);
                $subject = "Verifikasi email akun D'Kampoeng";
                if(sendMail($email, $message, $subject)){
                    $data['verified'] = 0;
                    $data += ['verification_token' => $token];
                    $user = $this->db->insert('users', $data);
                    response(['message' => 'Berhasil menambah user']);
                }
            }
            
        } else {
            $errors = $this->form_validation->error_array();
            if (!empty($upload['errors'])) {
                array_merge($errors, $upload['errors']);
            }

            response(['errors' => $errors]);
        }
    }

    public function upload($file)
    {
        $config = array();
        $photo = $file['photo']['name'];
        $file_ext = pathinfo($photo, PATHINFO_EXTENSION);
        $new_name = md5(uniqid(rand(), true)) . '.' . $file_ext;

        $config['upload_path'] = "./assets/admin/photos";
        $config['allowed_types'] = "jpg|jpeg|JPG|JPEG|png";
        $config['max_size'] = 1024;
        $config['file_name'] = $new_name;

        $this->load->library('upload', $config, 'file_upload');
        $this->file_upload->initialize($config);

        if ($this->file_upload->do_upload('photo')) {
            $this->file_upload->data();
            return array('file_name' => $new_name);
        } else {
            return array('errors' => ['photo' => $this->file_upload->display_errors()]);
        }
    }

    public function edit($id)
    {
        $user = $this->db->get_where('users', ['user_id' => $id])->result_array();

        $data['view'] = 'admin/user/edit';
        $data['scripts'] = 'admin/user/_scripts';
        $data['title'] = 'User - Edit Data';
        $data['menu'] = 'master';
        $data['sub'] = 'user';
        $data['user'] = $user[0];
        $this->load->view('admin/template/app', $data);
    }

    public function update($id)
    {
        if ($_FILES['photo']['name'] != "") {
            $upload = $this->upload($_FILES);
        }

        $data = [
            'name' => ucwords($this->input->post('name')),
            'email' => $this->input->post('email'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        if ($this->vm->userUpdate($id) && empty($upload['errors'])) {
            if (!empty($upload['file_name'])) {
                $data += ['photo' => $upload['file_name']];
                $photo = $this->bm->getField('users', 'photo', 'user_id', $id)->photo;
                if (file_exists("./assets/admin/photos/" . $photo)) {
                    unlink("./assets/admin/photos/" . $photo);
                }
                $_SESSION[SESSION_KEY]['photo'] = $upload['file_name'];
            }
            $user = $this->db->where('user_id', $id)->update('users', $data);
            $this->session->set_flashdata('notify', [
                'message' => "Berhasil mengubah user",
                'level' => "success",
            ]);
            response();
        } else {
            $errors = $this->form_validation->error_array();
            if (!empty($upload['errors'])) {
                array_merge($errors, $upload['errors']);
            }

            response(['errors' => $errors]);
        }
    }

    public function destroy($id)
    {
        if ($id != me()['user_id']) {
            $photo = $this->bm->getField('users', 'photo', 'user_id', $id)->photo;
            if(!is_null($photo)){
                if (file_exists("./assets/admin/photos/" . $photo)) {
                    unlink("./assets/admin/photos/" . $photo);
                }
            }
            $this->db->delete('users', ['user_id' => $id]);
            response(['id' => $id]);
        } else {
            response(['errors' => 'Gagal menghapus user, tidak dapat menghapus diri sendiri']);
        }

    }

    public function changePassword()
    {
        $data['view'] = 'admin/user/change_password';
        $data['scripts'] = 'admin/user/_scripts';
        $data['title'] = 'Ganti Password';
        $data['menu'] = '';
        $data['sub'] = '';
        $this->load->view('admin/template/app', $data);
    }
    
    public function change()
    {
        if($this->vm->changePassword()){
            $email = me()['email'];
            $password = $this->input->post('password');
            $this->db->where('email', $email)->update('users', ['password' => md5($password)]);
            response(['message' => 'Berhasil mengubah password']);
        }else{
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }
}
