<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['view'] = 'admin/product/index';
        $data['scripts'] = 'admin/product/_scripts';
        $data['title'] = 'Produk';
        $data['menu'] = 'master';
        $data['sub'] = 'product';
        $this->load->view('admin/template/app', $data);
    }

    public function productTable()
    {
        $query = "SELECT a.product_id, a.product_name, c.product_type_name, b.category_name, a.description, CONCAT('Rp. ', fRupiah(a.price)) AS price,
                    DATE_FORMAT(a.created_at, '%d/%m/%Y %H:%i:%s') AS created_at,
                    DATE_FORMAT(a.updated_at, '%d/%m/%Y %H:%i:%s') AS updated_at
                    FROM products as a
                    LEFT JOIN categories as b ON a.category_id = b.category_id
                    LEFT JOIN product_types as c ON b.product_type_id = c.product_type_id";

        $datatables = [
            'query' => $query,
            'column' => [
                'product_id',
                'product_name',
                'product_type_name',
                'category_name',
                'description',
                'price',
                'created_at',
                'updated_at',
            ],
            'searchOperator' => 'HAVING',
            'edit_url' => base_url('product/edit/'),
            'delete_url' => base_url('product/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus Produk : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'product_name', //Pilih salah satu dari table $column
            ],
            'action' => 'basic_action', //Level action
        ];

        showQueryTable($datatables);
    }

    public function create()
    {
        $data['view'] = 'admin/product/create';
        $data['scripts'] = 'admin/product/_scripts';
        $data['title'] = 'Produk - Tambah Data';
        $data['menu'] = 'master';
        $data['sub'] = 'product';
        $data['categories'] = $this->db->get('categories')->result();
        $this->load->view('admin/template/app', $data);
    }

    public function store()
    {
        
        if ($_FILES['photo']['name'] != "") {
            $upload = $this->upload($_FILES);
        }

        $e_verification = $this->input->post('verification');

        $data = [
            'product_name' => ucwords($this->input->post('product_name')),
            'category_id' => $this->input->post('category_id'),
            'description' => ucwords($this->input->post('description')),
            'price' => moneyClean($this->input->post('price')),
            'photo' => (!empty($upload['file_name'])) ? $upload['file_name'] : null,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        if ($this->vm->productStore() && empty($upload['errors'])) {
        
            $product = $this->db->insert('products', $data);
            response(['message' => 'Berhasil menambah produk']);
           
        } else {
            $errors = $this->form_validation->error_array();
            if (!empty($upload['errors'])) {
                array_merge($errors, $upload['errors']);
            }

            response(['errors' => $errors]);
        }
    }

    public function upload($file)
    {
        $config = array();
        $photo = $file['photo']['name'];
        $file_ext = pathinfo($photo, PATHINFO_EXTENSION);
        $new_name = md5(uniqid(rand(), true)) . '.' . $file_ext;

        $config['upload_path'] = "./assets/admin/products";
        $config['allowed_types'] = "jpg|jpeg|JPG|JPEG|png";
        $config['max_size'] = 1024;
        $config['file_name'] = $new_name;

        $this->load->library('upload', $config, 'file_upload');
        $this->file_upload->initialize($config);

        if ($this->file_upload->do_upload('photo')) {
            $this->file_upload->data();
            return array('file_name' => $new_name);
        } else {
            return array('errors' => ['photo' => $this->file_upload->display_errors()]);
        }
    }

    public function edit($id)
    {
        $product = $this->db->get_where('products', ['product_id' => $id])->result_array();

        $data['view'] = 'admin/product/edit';
        $data['scripts'] = 'admin/product/_scripts';
        $data['title'] = 'Produk - Edit Data';
        $data['menu'] = 'master';
        $data['sub'] = 'product';
        $data['product'] = $product[0];
        $data['categoryId'] = $product[0]['category_id'];
        $data['categories'] = $this->db->get('categories')->result();
        $this->load->view('admin/template/app', $data);
    }

    public function update($id)
    {
        if ($_FILES['photo']['name'] != "") {
            $upload = $this->upload($_FILES);
        }

        $data = [
            'product_name' => ucwords($this->input->post('product_name')),
            'category_id' => $this->input->post('category_id'),
            'description' => ucwords($this->input->post('description')),
            'price' => moneyClean($this->input->post('price')),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        if ($this->vm->productUpdate($id) && empty($upload['errors'])) {
            if (!empty($upload['file_name'])) {
                $data += ['photo' => $upload['file_name']];
                $photo = $this->bm->getField('products', 'photo', 'product_id', $id)->photo;
                if (file_exists("./assets/admin/products/" . $photo)) {
                    unlink("./assets/admin/products/" . $photo);
                }
                $_SESSION[SESSION_KEY]['photo'] = $upload['file_name'];
            }
            $product = $this->db->where('product_id', $id)->update('products', $data);
            $this->session->set_flashdata('notify', [
                'message' => "Berhasil mengubah produk",
                'level' => "success",
            ]);
            response();
        } else {
            $errors = $this->form_validation->error_array();
            if (!empty($upload['errors'])) {
                array_merge($errors, $upload['errors']);
            }

            response(['errors' => $errors]);
        }
    }

    public function destroy($id)
    {
        $photo = $this->bm->getField('products', 'photo', 'product_id', $id)->photo;
        if(!is_null($photo)){
            if (file_exists("./assets/admin/products/" . $photo)) {
                unlink("./assets/admin/products/" . $photo);
            }
        }
        $this->db->delete('products', ['product_id' => $id]);
        response(['id' => $id]);
        
    }
    
}
