<?php defined('BASEPATH') or exit('No direct script access allowed');

function menu($name, $menu, $fa, $display)
{
    $active = ($name == $menu) ? 'active' : '';
    $href = base_url($name);
    echo "<li class = '$active'><a href='$href' ><i class='$fa'></i>$display</a> </li>";
}

function m_active($name, $menu)
{
    echo ($name == $menu) ? '<li class = "active">' : "<li>";
}

function sub($name, $sub, $display, $menu, $menu_name)
{
    $activeMenu = ($menu == $menu_name) ? "style = 'display: block'" : "";
    $activeStyle = ($name == $sub) ? "class = 'active'" : "";
    $href = base_url($name);

    $html = '';
    $html .= "<ul class='nav child_menu' $activeMenu>";
    $html .= "<li $activeStyle style = 'display: block'><a href='$href'>$display</a></li></ul>";
    
    echo $html;
}

function _action($data) {
    if($data['action'] == 'basic_action'){
        
        return  '<a title="Edit" class="btn btn-info btn-xs"
        href="'.$data['edit_url'].'"><i class="fa fa-pencil-square-o"></i></a>|

        <a id="form-'.$data['id'].'" title="Hapus" class="btn btn-danger btn-xs form-delete"
        data-id="'.$data['id'].'" data-url="'.$data['delete_url'].'" data-confirm="'.$data['confirm'].'"><i class="fa fa-trash-o"></i></a>';
    
    }else if($data['action'] == 'order_complete'){
        
        return '<a id="form-'.$data['id'].'" title="Revisi" class="btn btn-warning btn-xs form-delete"
        data-id="'.$data['id'].'" data-url="'.$data['edit_url'].'" data-confirm="'.$data['confirm'].'"><i class="fa fa-refresh"></i></a>|

        <a id="form-'.$data['id'].'" title="Hapus" class="btn btn-danger btn-xs form-delete"
        data-id="'.$data['id'].'" data-url="'.$data['delete_url'].'" data-confirm="'.$data['confirm'].'"><i class="fa fa-trash-o"></i></a>|

        <a title="Detail Order" class="btn btn-info btn-xs"
        href="'.base_url('order_complete/detail_order/'.$data['id']).'"><i class="fa fa-eye"></i></a>|

        <a onclick="kwitansi(' . "'" . base_url('order/kwitansi/'.$data['id']) . "'" . ');" class="btn btn-xs btn-default"><i class="fa fa-money"> Kwitansi</i></a>';
    
    }else if($data['action'] == 'order_revision'){
        return '<a title="Edit" class="btn btn-success btn-xs"
        href="'.$data['edit_url'].'"><i class="fa fa-check-square-o"></i></a>';
    }else if($data['action'] == 'orderlist_action'){
        
        return  '<a title="Ganti Status Pesanan" class="btn btn-warning btn-xs"
        onclick="changeStatus(' . "'" . $data['id'] . "'" . ');"><i class="fa fa-info-circle"></i></a>|

        <a title="Edit" class="btn btn-info btn-xs"
        href="'.$data['edit_url'].'"><i class="fa fa-pencil-square-o"></i></a>|

        <a id="form-'.$data['id'].'" title="Hapus" class="btn btn-danger btn-xs form-delete"
        data-id="'.$data['id'].'" data-url="'.$data['delete_url'].'" data-confirm="'.$data['confirm'].'"><i class="fa fa-trash-o"></i></a>';
    
    }
}
