<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Query_model extends CI_Model
{

    public function getCategoriesQty(Type $var = null)
    {
        $sql = "SELECT a.*,
                    (SELECT COUNT(b.product_id)
                        FROM products b
                        WHERE a.category_id = b.category_id) as total_product
                FROM categories a";
        
        return $this->db->query($sql)->result();
    }

    public function getDetailOrderList($id)
    {
        return $this->db->select('a.*, b.table_name')
                            ->from('customer_orders as a')
                            ->join('table_numbers as b', 'a.table_id = b.table_id')
                            ->where('a.order_id', $id)
                            ->get()->row();
    }

    public function getOrderStatus($status)
    {
        return $this->db->select('a.*, b.table_name')
                            ->from('customer_orders as a')
                            ->join('table_numbers as b', 'a.table_id = b.table_id')
                            ->where('a.status', $status)
                            ->get()->row();
    }

    public function getTablesNotInOrders()
    {
        $sql = "SELECT * FROM table_numbers 
                    WHERE table_id 
                    NOT IN (SELECT table_id FROM customer_orders WHERE status = 1 OR status = 2) ORDER BY table_name ASC";
        return $this->db->query($sql)->result();
    }

    public function updateTotalOrder($price, $orderId)
    {
        $sql = "UPDATE customer_orders set total_order = total_order $price WHERE order_id = $orderId";
        $this->db->query($sql);
        return $this->db->select('total_order')
                            ->from('customer_orders')
                            ->where('order_id', $orderId)
                            ->limit(1)
                            ->get()
                            ->row()
                            ->total_order;
    }

    public function updateTotalOrderRevision($price, $orderId)
    {
        $sql = "UPDATE customer_orders set total_order = total_order $price, extra = extra $price WHERE order_id = $orderId";
        $this->db->query($sql);
        return $this->db->select('total_order, extra')
                            ->from('customer_orders')
                            ->where('order_id', $orderId)
                            ->limit(1)
                            ->get()
                            ->row();
    }

    public function getTablePayment()
    {
        return $this->db->select('a.order_id, a.table_id, b.table_name')
                            ->from('customer_orders as a')
                            ->join('table_numbers as b', 'a.table_id = b.table_id')
                            ->where('a.status', 2)
                            ->order_by('table_name', 'ASC')
                            ->get()->result();
    }

    public function getDetailTableId($tableId)
    {
        return $this->db->select('a.*, DATE_FORMAT(a.created_at, "%d/%m/%Y %H:%i:%s") AS created_at,  b.table_name')
                            ->from('customer_orders as a')
                            ->join('table_numbers as b', 'a.table_id = b.table_id')
                            ->where(['a.status' => 2, 'a.table_id' => $tableId])
                            ->get()->row();
    }

    public function getDetailOrderId($orderId)
    {
        return $this->db->select('a.*, b.product_name')
                            ->from('detail_orders as a')
                            ->join('products as b', 'a.product_id = b.product_id')
                            ->where('a.order_id', $orderId)
                            ->get()->result();
    }

    public function getDetailOrderId2($orderId)
    {
        return $this->db->select('a.*, DATE_FORMAT(a.created_at, "%d/%m/%Y %H:%i:%s") AS created_at,  b.table_name')
                            ->from('customer_orders as a')
                            ->join('table_numbers as b', 'a.table_id = b.table_id')
                            ->where('a.order_id', $orderId)
                            ->get()->row();
    }

    public function getDetailOrderId3($orderId)
    {
        return $this->db->select('a.*, b.product_name')
                            ->from('detail_orders as a')
                            ->join('products as b', 'a.product_id = b.product_id')
                            ->where('a.order_id', $orderId)
                            ->get()->result();
    }

}
