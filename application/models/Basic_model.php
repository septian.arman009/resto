<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Basic_model extends CI_Model
{

    public function getField($table, $field, $where, $key)
    {
        return $this->db->select($field)->from($table)->where($where, $key)->limit(1)->get()->row();
    }

    public function getWhere($table, $data)
    {
        return $this->db->select("*")->from($table)->where($data)->limit(1)->get();
    }

    public function getCategoriesQty(Type $var = null)
    {
        return $this->db->query(
            "SELECT a.*,
                (SELECT COUNT(b.product_id)
                    FROM products b
                    WHERE a.category_id = b.category_id) as total_product
            FROM categories a")
            ->result();
    }

}
