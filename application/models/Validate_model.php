<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Validate_model extends CI_Model
{

    public function userStore()
    {
        $this->load->library('form_validation');
        $rules = [
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|is_unique[users.email]',
                'errors' => [
                    'is_unique' => '* Email sudah digunakan',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function userUpdate($id)
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|editUnique[users.email.' . $id . '.user_id]',
                'errors' => [
                    'editUnique' => '* Email sudah terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function login($password)
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|exist[users.email]|checkPasswordAuth['.$password.']',
                'errors' => [
                    'exist' => 'Email tidak terdaftar',
                    'checkPasswordAuth' => 'Password salah'
                ],
            ]
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function sendLink()
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|exist[users.email]',
                'errors' => [
                    'exist' => 'Email tidak terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function resetPassword()
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|valid_email|exist[users.email]',
                'errors' => [
                    'exist' => 'Email tidak terdaftar',
                    'valid_email' => 'Email tidak valid',
                ],
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|min_length[8]',
                'errors' => [
                    'min_length' => 'Password minimal adalah 8 karakter',
                ],
            ],
            [
                'field' => 'confirm',
                'label' => 'Confirm',
                'rules' => 'trim|matches[password]',
                'errors' => [
                    'matches' => 'Password tidak sama',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function changePassword()
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'old_password',
                'label' => 'Old Password',
                'rules' => 'trim|checkPassword',
                'errors' => [
                    'checkPassword' => '* Password salah',
                ],
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|min_length[8]',
                'errors' => [
                    'min_length' => '* Password minimal adalah 8 karakter',
                ],
            ],
            [
                'field' => 'confirm',
                'label' => 'Confirm',
                'rules' => 'trim|matches[password]',
                'errors' => [
                    'matches' => '* Password tidak sama',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function categoryStore()
    {
        $this->load->library('form_validation');
        $rules = [
            [
                'field' => 'category_name',
                'label' => 'Category Name',
                'rules' => 'trim|is_unique[categories.category_name]',
                'errors' => [
                    'is_unique' => '* Kategori sudah digunakan',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function categoryUpdate($id)
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'category_name',
                'label' => 'Category Name',
                'rules' => 'trim|editUnique[categories.category_name.' . $id . '.category_id]',
                'errors' => [
                    'editUnique' => '* Kategori sudah terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function productTypeStore()
    {
        $this->load->library('form_validation');
        $rules = [
            [
                'field' => 'product_type_name',
                'label' => 'Product Type Name',
                'rules' => 'trim|is_unique[product_types.product_type_name]',
                'errors' => [
                    'is_unique' => '* Jenis produk sudah digunakan',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function productTypeUpdate($id)
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'product_type_name',
                'label' => 'Product Type Name',
                'rules' => 'trim|editUnique[product_types.product_type_name.' . $id . '.product_type_id]',
                'errors' => [
                    'editUnique' => '* Jenis produk sudah terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function productStore()
    {
        $this->load->library('form_validation');
        $rules = [
            [
                'field' => 'product_name',
                'label' => 'Product Name',
                'rules' => 'trim|is_unique[products.product_name]',
                'errors' => [
                    'is_unique' => '* Nama produk sudah digunakan',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function productUpdate($id)
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'product_name',
                'label' => 'Product Name',
                'rules' => 'trim|editUnique[products.product_name.' . $id . '.product_id]',
                'errors' => [
                    'editUnique' => '* Nama produk sudah terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function orderStore()
    {
        $this->load->library('form_validation');
        $rules = [
            [
                'field' => 'customer_name',
                'label' => 'Customer Name',
                'rules' => 'trim|required',
                'errors' => [
                    'required' => '* Nama customer tidak boleh kosong',
                ],
            ],
            [
                'field' => 'table_id',
                'label' => 'Table Id',
                'rules' => 'trim|required',
                'errors' => [
                    'required' => '* Nomor meja tidak boleh kosong',
                ],
            ]
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

}
