<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Datatables_model extends CI_Model
{

    public function oneTable($dt)
    {
        $columns = implode(', ', $dt['col-display']);
        $sql = "SELECT {$columns} FROM {$dt['table']}";
        $this->show($dt, $sql);
    }

    public function queryTable($dt)
    {
        $sql = $dt['query'];
        $this->show($dt, $sql);
    }

    public function show($dt, $sql)
    {
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        $search = $dt['search']['value'];
        $where = '';

        

        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                break;
            }
        }

        if ($where != '') {
            if(!empty($dt['searchOperator'])){
                $searchOperator = $dt['searchOperator'];
                $sql .= " $searchOperator " . $where;
            }else{
                $sql .= " WHERE " . $where;
            }
            
        }

        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);

        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        foreach ($list->result_array() as $row => $val) {
            $rows = array();
            $id = null;
            $data = 1;
            $confirm_data = '';
            foreach ($columnd as $key => $kolom) {

                if (is_null($id)) {
                    $id = $val[$kolom];
                    $rows[] = $id;
                }else if($dt['action'] == 'orderlist_action' && $data == 6){

                    if($val[$kolom] == 'Sedang diproses'){
                        $rows[] = '<p style="color: orange; font-weight:bold">'.$val[$kolom].'<?p>';
                    }else if($val[$kolom] == 'Pesanan diantar'){
                        $rows[] = '<p style="color: green; font-weight:bold">'.$val[$kolom].'<?p>';
                    }else{
                        $rows[] = '<p>'.$val[$kolom].'<?p>';
                    }

                } else {
                    if($val[$kolom]){
                        $rows[] = "<p id='$id'>$val[$kolom]</p>";
                    }else{
                        $rows[] = "-";
                    }
                    
                }

                if($kolom == $dt['confirm']['c_column']){
                    $confirm_data = $dt['confirm']['message'].''.$val[$kolom];
                }
    
                $data++;
            }
            
            
            $rows[] = _action([
                'action' => $dt['action'],
                'edit_url' => $dt['edit_url'].$id,
                'delete_url' => $dt['delete_url'].$id,
                'confirm' => ($confirm_data == '') ? $dt['confirm']['message'] : $confirm_data,
                'id' => $id
            ]);
               
            $option['data'][] = $rows;

        }
        echo json_encode($option);
    }
}