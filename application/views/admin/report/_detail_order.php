<div class="row" style="padding: 20px">
    <div class="col-md-12">
        <table class="table">
            <thead>
                <tr>
                    <th>Nama Produk</th>
                    <th>Harga</th>
                    <th>Jumlah</th>
                    <th>Sub Total</th>
                </tr>
            </thead>
            <?php foreach ($details as $detail) { ?>
            <tr>
                <td><?= $detail->product_name ?></td>
                <td><?= toRp($detail->order_price) ?></td>
                <td>x <?= $detail->qty ?></td>
                <td><?= toRp($detail->sub_total) ?></td>
            </tr>
            <?php } ?>
        </table>
    </div>

    <div class="col-md-12">
        <?php flash() ?>
        <input type="number" value="<?= (!is_null($order)) ? $order->order_id : '' ?>" id="order_id"
            style="display: none">
        <div class="animated flipInY">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-cutlery"></i>
                </div>
                <div class="count"><?= $order->customer_name ?></div>

                <h3><?= $order->table_name ?></h3>
                <p style="font-size: 20px" id="total-order">Total Pesanan <a class="pull-right"
                        style="margin-right: 5px"> <?= toRp($order->total_order) ?></a></p>
                <p style="font-size: 20px">Bayar <a class="pull-right" style="margin-right: 5px">
                        <?= toRp($order->pay) ?></a></p>
                <p style="font-size: 20px">Kembalian <a class="pull-right" style="margin-right: 5px">
                        <?= toRp($order->money_back) ?></a></p>
                <p style="font-size: 20px">Kembalian (Revisi) <a class="pull-right" style="margin-right: 5px">
                        <?= toRp($order->money_back_revision) ?></a></p>
                <p style="font-size: 20px" id="extra">Extra <a class="pull-right" style="margin-right: 5px">
                        <?= toRp($order->extra) ?></a></p>
            </div>
        </div>

        <div class="animated flipInY">
            <div class="tile-stats">
                <p>Revisi : <?= $order->revision ?></p>
            </div>
        </div>
    
    </div>
</div>