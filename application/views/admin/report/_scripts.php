<script src="<?= base_url() ?>assets/admin/vendors/raphael/raphael.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendors/morris.js/morris.min.js"></script>
<script>
    $(document).ready(function () {

        Morris.Bar({
            element: 'income',
            data: <?= json_encode($income) ?>,
            xkey: ['month'],
            ykeys: ['income'],
            labels: ['Total Penjualan'],
            barRatio: 0.4,
            barColors: ['#26B99A'],
            xLabelAngle: 50,
            hideHover: 'auto',
            resize: true
        });

        Morris.Bar({
            element: 'income-day',
            data: <?= json_encode($income_day) ?>,
            xkey: ['day'],
            ykeys: ['income_day'],
            labels: ['Total Penjualan'],
            barRatio: 0.2,
            barColors: ['#26B99A'],
            xLabelAngle: 50,
            hideHover: 'auto',
            resize: true
        });
        
    });

    function changeData() {
        let month = $("#month").val();
        let year = $("#year").val();
        window.location = "<?= base_url() ?>statistic/"+month+"/"+year;
    }

    function detail(orderId) {
        $("#md-modal").modal('show');
        $(".modal-title").html("Detail Transaksi");
		let url = "<?= base_url() ?>statistic/detail_order/"+orderId;
		loadView(url, '.modal-custom');
    }

    function kwitansi(pageURL, title,w,h) {
        var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/2);
        var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    }

</script>