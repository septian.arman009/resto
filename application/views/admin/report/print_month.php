<?php

$this->fpdf->FPDF_17('P', 'cm', 'A4');
$this->fpdf->AliasNbPages();
$this->fpdf->AddPage();

$this->fpdf->Image('assets/admin/images/no_image.jpg', 1, 1, 2.5, 0, '', base_url('admin'));

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 24);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, "D'Kampoeng Resto", 0, 0, 'C');

$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.8);
$this->fpdf->Cell(0, 0, 'Jl. Jalan Kiara Endah No.23, Bojong Kulur, Gunung Putri, Bogor,Jawa Barat.', 0, 0, 'C');
$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, 'Telp. 0812-9499-9595', 0, 0, 'C');

$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.7, 20, 3.6);
$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.8, 20, 3.7);

$this->fpdf->SetFont('Times', 'B', 12);
$this->fpdf->Ln(0.6);
$this->fpdf->Cell(0, 0,"Laporan Pendapatan Bulan : ".toIndoMonth($month), 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->ln(0.5);
$this->fpdf->Cell(1, 0.5, 'No', 1, 0, 'C');
$this->fpdf->Cell(6, 0.5, 'Nama Customer', 1, 0, 'C');
$this->fpdf->Cell(6, 0.5, 'Taggal Pemesanan', 1, 0, 'C');
$this->fpdf->Cell(6, 0.5, 'Tagihan', 1, 0, 'C');

$this->fpdf->Ln();
$no = 1;
$total = 0;
foreach ($orders as $order) {
    $this->fpdf->SetFont('Times', '', 11);
    $this->fpdf->Cell(1, 0.5, $no++, 1, 0, 'C');
    $this->fpdf->Cell(6, 0.5, $order->customer_name, 1, 0, 'L');
    $this->fpdf->Cell(6, 0.5, $order->created_at, 1, 0, 'L');
    $this->fpdf->Cell(6, 0.5, toRp($order->total_order), 1, 0, 'L');
    $this->fpdf->Ln();
    $total += $order->total_order;
}

    $this->fpdf->SetFont('Times', '', 11);
    $this->fpdf->Cell(13, 0.5, 'Total Pendapatan', 1, 0, 'R');
    $this->fpdf->Cell(6, 0.5, toRp($total), 1, 0, 'L');
    $this->fpdf->Ln();

$this->fpdf->Output();
