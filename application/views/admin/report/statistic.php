<div class="page-title">
	<div class="title_left">
		<h3>Statistik <small> Penjualan </small></h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Data Penjualan Tahun <?= $year ?></h2>
					<div class="clearfix"></div>
				</div>

				<div class="x_content">
					<div class="row">
						<div class="col-md-3">
							<select id="year" class="form-control" onchange="changeData()">
								<?php for ($i=2019; $i <= 2099 ; $i++) {
								$selected = ($year == $i) ? "selected" : "";
								echo '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
							}?>
							</select>
							<br>
							<a href="<?= base_url('statistic/print_year/'.$year) ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Cetak Laporan <?= $year ?></a>

						</div>
						<div class="col-md-3">
							<select id="month" class="form-control" onchange="changeData()">
								<?php for ($j=1; $j <= 12 ; $j++) {
								$selected = ($month == $j) ? "selected" : "";
								echo '<option '.$selected.' value="'.$j.'">'.toIndoMonth($j).'</option>';
							}?>
							</select>
							<br>
							<a href="<?= base_url('statistic/print_month/'.$year.'/'.$month) ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Cetak Laporan <?= toIndoMonth($month) ?></a>
						</div>

					</div>

					<div class="row">

						<div class="col-md-6">
							<div class="ln_solid"></div>
							Data Penjualan Tahun <?= $year ?>
							<div id="income"></div>
						</div>
						<div class="col-md-6">
							<div class="ln_solid"></div>
							Data Penjualan Bulan <?= toIndoMonth($month) ?>
							<div id="income-day"></div>
						</div>
					</div>
					<div class="ln_solid"></div>

					<div class="row">
						<div class="com-md-12">
							<table class="table table-striped table-hover" id="table-detail">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Cutsomer</th>
										<th>Tagihan</th>
										<th>Tanggal Transaksi</th>
										<th width="20%"></th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1; foreach ($details as $detail) { ?>
										<tr>
											<td><?= $no++; ?></td>
											<td><?= $detail->customer_name ?></td>
											<td><?= toRp($detail->total_order) ?></td>
											<td><?= $detail->created_at ?></td>
											<td>
												<a onclick="detail('<?= $detail->order_id ?>')" class="btn btn-success"><i class="fa fa-eye"> Detail</i></a>
												<a onclick="kwitansi('<?= base_url('order/kwitansi/'.$detail->order_id)?>', 'Kwitansi',480,600)" class="btn btn-default"><i class="fa fa-money"> Kwitansi</i></a>
											</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

</div>