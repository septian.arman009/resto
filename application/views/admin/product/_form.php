<div class="item form-group group-product_name">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Produk</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($product)) echo $product['product_name'] ?>" class="form-control col-md-7 col-xs-12"
			name="product_name" placeholder="ex. Gurame Saus Asam" required="required" type="text">
		<div id="response-product_name" class="response-error"></div>
	</div>
</div>

<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="category_id" class="form-control" required="required">
		<option value="">- Pilih Jenis Kategori -</option>
			<?php
				foreach ($categories as $category) {
					if(!is_null($categoryId) && $category->category_id == $categoryId){
						echo "<option selected value='$category->category_id'>$category->category_name</option>";
					}else{
						echo "<option value='$category->category_id'>$category->category_name</option>";
					}
                }
			?>
		</select>
	</div>
</div>

<div class="item form-group group-description">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<textarea class="form-control col-md-7 col-xs-12" name="description"><?php if(!empty($product)) echo $product['description'] ?></textarea>
	</div>
</div>

<div class="item form-group group-price">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Harga</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?= (!empty($product)) ? toRp($product['price']) : 'Rp. 0' ?>" class="form-control col-md-7 col-xs-12"
			name="price" id="price" required="required" type="text">
		<div id="response-price" class="response-error"></div>
	</div>
</div>

<div class="item form-group group-photo">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Foto</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input id="photo" class="form-control col-md-7 col-xs-12" name="photo" type="file">
		<span id="response-photo" class="response-error"></span>
	</div>
</div>

<?php if(!empty($product) && !is_null($product['photo'])){ ?>
<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<img src="<?= base_url('assets/admin/products/'.$product['photo']) ?>" alt="Photo" width="200px" height="200px"
			style="border-radius: 10px">
	</div>
</div>
<?php } ?>

<div class="ln_solid"></div>
<div class="form-group">
	<div class="col-md-6 col-md-offset-3">
		<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
	</div>
</div>