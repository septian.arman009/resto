<!-- validator -->
<script src="<?= base_url() ?>assets/admin/vendors/validator/validator.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<script src="<?= base_url() ?>assets/admin/js/jquery.maskMoney.js"></script>

<script>
	$(document).ready(function () {
		detailOrder();
		if (document.body.contains(document.getElementById('add_payment'))) {
			$('#add_payment').maskMoney({
				prefix: 'Rp. ',
				thousands: '.',
				decimal: ',',
				precision: 0
			});
		}

	});

	function searchMenu(categoryId) {
		loadView("<?= base_url() ?>order/search_menu_category/" + categoryId, "#menu-list");
		$(".cat-active").removeClass('cat-active');
		$("#category-" + categoryId).addClass('cat-active');
	}

	function addMenu(productId) {
		let orderId = $("#order_id").val();
		doSomething("<?= base_url() ?>revision/add_menu_revision/" + orderId + '/' + productId);
		setTimeout(() => {
			detailOrder();
		}, 500);
	}

	function removeMenu(productId) {
		let orderId = $("#order_id").val();
		if (orderId != "") {
			doSomething("<?= base_url() ?>revision/remove_menu_revision/" + orderId + '/' + productId);
			setTimeout(() => {
				detailOrder();
			}, 500);
		} else {
			notify('Terjadi Kesalahan', 'Data customer masih kosong', 'warning');
		}
	}

	function detailOrder() {
		let orderId = $("#order_id").val();
		if (orderId != "") {
			loadView("<?= base_url() ?>order/detail_order/" + orderId, "#detail-order");
		}

	}

	function custom_response($form, rule, message) {
		if (rule == 'custom_store') {
			location.reload();
		}
	}

	function finish(orderId) {

		swal({
            title: "Konfirmasi",
            text: "Apakah revisi sudah selesai ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ya",
            closeOnConfirm: false
        },
        function () {
            let url = '<?= base_url()?>revision/finish/'+orderId;
            let type = 'post';
            postJson(url, type, data = {}, function (err, response) {
                if (response) {
                    window.location = "<?= base_url('revision') ?>";
                } else {
                    console.log('Error : ', err);
                }
            });
        });

	}

	function returnBack(){
		window.location = '<?= base_url('revision') ?>';
	}
</script>

<style>
	#categories,
	#order-list {
		border: 1px solid #ccc;
		border-radius: 5px;
	}

	.cat-active {
		font-size: 24px;
		color: #1ABB9C;
	}

	a:focus,
	a:hover {
		color: #1ABB9C;
	}

	.flipInY {
		cursor: pointer;
	}
</style>