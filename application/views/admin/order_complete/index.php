<div class="page-title">
	<div class="title_left">
		<h3>Pesanan Selesai</h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Daftar Pesanan Selesai</h2>
					<div class="clearfix"></div>
				</div>
				<?php flash() ?>    
				<div class="x_content table-responsive">
					<table id="order-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th id="th" width="10%">ID</th>
								<th id="th">Nama Customer</th>
								<th id="th">Nomor Meja</th>
								<th id="th">Tanggal Pesanan</th>
								<th id="th">Total Tagihan</th>
								<th id="th" class="no-sort" width="20%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">ID</th>
								<th class="footer">Nama Customer</th>
								<th class="footer">Nomor Meja</th>
								<th class="footer">Tanggal Pesanan</th>
								<th class="footer">Total Tagihan</th>
								<th class="footer"></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>