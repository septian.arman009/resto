<div class="page-title">
	<div class="title_left">
		<h3>Pemesanan <small> (Order) </small></h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Pemesanan</h2>
					<div class="clearfix"></div>
				</div>

				<div class="x_content">
					<div class="row">
						<div class="col-md-6">
							<table class="table">
								<thead>
									<tr>
										<th>Nama Produk</th>
										<th>Harga</th>
										<th>Jumlah</th>
										<th>Sub Total</th>
									</tr>
								</thead>
								<?php foreach ($details as $detail) { ?>
								<tr>
									<td><?= $detail->product_name ?></td>
									<td><?= toRp($detail->order_price) ?></td>
									<td>x <?= $detail->qty ?></td>
									<td><?= toRp($detail->sub_total) ?></td>
								</tr>
								<?php } ?>
							</table>
						</div>

						<div class="col-md-6">
							<?php flash() ?>
							<input type="number" value="<?= (!is_null($order)) ? $order->order_id : '' ?>" id="order_id"
								style="display: none">
							<div class="animated flipInY">
								<div class="tile-stats">
									<div class="icon"><i class="fa fa-cutlery"></i>
									</div>
									<div class="count"><?= $order->customer_name ?></div>

									<h3><?= $order->table_name ?></h3>
									<p style="font-size: 20px" id="total-order">Total Pesanan <a class="pull-right"
											style="margin-right: 5px"> <?= toRp($order->total_order) ?></a></p>
									<p style="font-size: 20px">Bayar <a class="pull-right" style="margin-right: 5px">
											<?= toRp($order->pay) ?></a></p>
									<p style="font-size: 20px">Kembalian <a class="pull-right"
											style="margin-right: 5px"> <?= toRp($order->money_back) ?></a></p>
									<p style="font-size: 20px">Kembalian (Revisi) <a class="pull-right"
											style="margin-right: 5px"> <?= toRp($order->money_back_revision) ?></a></p>
									<div class="animated flipInY" style="padding: 5px">
										<div class="tile-stats">
											<p>Revisi : <?= $order->revision ?></p>
										</div>
									</div>
									<p style="font-size: 20px" id="extra">Extra <a class="pull-right"
											style="margin-right: 5px"> <?= toRp($order->extra) ?></a></p>
								</div>
								<div class="pull-right">
									<a class="btn btn-default" href="<?= base_url('order_complete') ?>">Kembali</a>
								</div>
							</div>

							

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>