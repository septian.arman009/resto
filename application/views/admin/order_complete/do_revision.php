<div class="page-title">
	<div class="title_left">
		<h3>Pesanan <small> (Order) </small></h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Pesanan</h2>
					<div class="clearfix"></div>
				</div>

				<div class="x_content">
					<div class="row">
						<div class="col-md-6">
							<div id="detail-order"></div>
						</div>

						<div class="col-md-6">
							<?php flash() ?>
							<input type="number" value="<?= $order->order_id ?>" id="order_id" style="display: none">
							<div class="animated flipInY">
								<div class="tile-stats">
									<div class="icon"><i class="fa fa-cutlery"></i>
									</div>
									<div class="count"><?= $order->customer_name ?></div>

									<h3><?= $order->table_name ?></h3>
									<p style="font-size: 20px" id="total-order">Total Pesanan <a class="pull-right" style="margin-right: 5px"> <?= toRp($order->total_order) ?></a></p>
									<p style="font-size: 20px">Bayar <a class="pull-right" style="margin-right: 5px"> <?= toRp($order->pay) ?></a></p>
									<p style="font-size: 20px">Kembalian <a class="pull-right" style="margin-right: 5px"> <?= toRp($order->money_back) ?></a></p>
									<p style="font-size: 20px">Kembalian (Revisi) <a class="pull-right" style="margin-right: 5px"> <?= toRp($order->money_back_revision) ?></a></p>
									<div class="animated flipInY" style="padding: 5px">
										<div class="tile-stats">
											<p>Revisi : <?= $order->revision ?></p>
										</div>
									</div>
									<p style="font-size: 20px" id="extra">Extra <a class="pull-right" style="margin-right: 5px"> <?= toRp($order->extra) ?></a></p>
								</div>
								
								<div class="pull-right">
									<button id="add-money-back" <?= ($order->extra > 0 || $order->extra == 0) ? 'style="display:none"' : '' ?> class="btn btn-warning form-delete" data-url="<?= base_url() ?>revision/add_money_back/<?= $order->order_id ?>">Tambahkan Kembalian</button>
									<button id="finish" <?= ($order->extra == 0) ? '' : 'style="display:none"' ?> class="btn btn-success" onclick="finish('<?= $order->order_id ?>')">Selesai</button>
									<a class="btn btn-default" href="<?= base_url('revision') ?>">Kembali</a>
								</div>
							
								<form class="form-horizontal form-label-left form-data" id="add-payment" <?= ($order->extra < 0 || $order->extra == 0) ? 'style="display:none"' : '' ?>
									action="<?= base_url('revision/add_payment/'. $order->order_id) ?>"
									method="post"
									enctype="multipart/form-data"
									data-rule="default_update"
									data-redirect="<?= base_url('revision/do_revision/'. $order->order_id) ?>"
									data-btn="#btn-save"
									btn_response="Tambah Pembayaran">

									<div class="item form-group group-add_payment">
										<div class="col-md-12">
											<input value="Rp. 0" class="form-control col-md-12"
												name="add_payment" id="add_payment" required="required" type="text">
											<div id="response-add_payment" class="response-error"></div>
										</div>
									</div>

									<div class="ln_solid"></div>
									<div class="form-group">
										<div class="col-md-12">
											<button id="btn-save" type="submit" class="btn btn-success">Tambah Pembayaran</button>
										</div>
									</div>

								</form>
									
								
							</div>
						</div>
					</div>

					<div class="ln_solid"></div>

					<div class="row">
						<div class="col-md-6">
							<h4>Kategori</h4>
							<div class="ln_solid"></div>

							<ul class="list-unstyled timeline">

								<?php foreach ($types as $type) { ?>
								<li>
									<div class="block">
										<div class="tags">
											<a class="tag">
												<span><?= $type->product_type_name ?></span>
											</a>
										</div>

										<?php foreach ($categories as $category) { 
												if($type->product_type_id == $category->product_type_id){?>

										<div class="block_content">
											<h2 class="title">
												<a id="category-<?= $category->category_id ?>" onclick="searchMenu('<?= $category->category_id ?>')"><?= $category->category_name ?></a>
											</h2>
											<div class="byline">
												<span><?= $category->total_product ?> Menu</span> by <a>D'Kampoeng</a>
											</div>
										</div>

										<?php } } ?>

									</div>
								</li>
								<?php } ?>

							</ul>
						</div>
						<div class="col-md-6">
							<h4>Daftar Menu</h4>
							<div class="ln_solid"></div>
							<ul class="list-unstyled msg_list">
								<div id="menu-list">
									<li>
										<a>
											<span class="image">
												<img style="width: 100px" src="<?= base_url('assets/admin/images/no_image.jpg') ?>" alt="img">
											</span>
											<span>
												<span style="font-size: 18px">ex. Gurame...</span>
												<span style="font-size: 18px"
													class="time">Rp. 15.0...</span>
											</span>
											<span class="message">
												1 Gurame, Sambal...
											</span>
										</a>
									</li>
								</div>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>