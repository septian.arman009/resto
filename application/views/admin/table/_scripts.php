<!-- validator -->
<script src="<?= base_url() ?>assets/admin/vendors/validator/validator.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<script>
	$(document).ready(function () {
		if (document.body.contains(document.getElementById('table-table'))) {
			$('#table-table tfoot th').each(function () {
				var title = $(this).text();
				if (title != '')
					var inp = '<input type="text" class="form-control footer-s" placeholder="' + title +
						'" id="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#table-table').DataTable({
				"processing": false,
				"serverSide": true,
				"order": [
					[0, 'desc']
				],
				"ajax": {
					"url": '<?= base_url('table/table') ?>',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
		}
	});

	function custom_response($form, rule, message) {
		notify('Sukses', 'Berhasil menambah angkatan', 'success');
		$form.trigger('reset');
		$("#table-"+message).remove();
	}
</script>

<style>
	#table-table_filter {
		display: none;
	}
</style>