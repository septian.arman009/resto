<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Meja </label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="input-group">
			<span class="input-group-btn">
				<button type="button" class="btn btn-default">Meja </button>
			</span>
			<select name="table_name" class="form-control">
			<?php
				for ($i=1; $i <= 16; $i++) { 
					if(!in_array($i, $tbNumber)){
						if(!empty($tableNumber)){
							$selected = ($i == $tableNumberPosition) ? "selected" : "";
						}else{
							$selected = "";
						}
						echo "<option $selected id='table-$i' value='$i'>$i</option>";                        
					}
				}
			?>
			</select>
		</div>
	</div>
</div>

<div class="ln_solid"></div>
<div class="form-group">
	<div class="col-md-6 col-md-offset-3">
		<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
	</div>
</div>