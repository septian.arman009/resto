<div class="page-title">
	<div class="title_left">
		<h3>Nomor Meja</h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Daftar Nomor Meja</h2>
					<div class="clearfix"></div>
				</div>
				<?php flash() ?>
				<a href="<?= base_url('table/create') ?>" class="btn btn-default">Tambah Data</a>
				<hr>
				<div class="x_content table-responsive">
					<table id="table-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th id="th" width="10%">ID</th>
								<th id="th">Nomor Meja</th>
								<th id="th">Dibuat</th>
								<th id="th">Update Terakhir</th>
								<th id="th" class="no-sort" width="10%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">ID</th>
								<th class="footer">Nomor Meja</th>
								<th class="footer">Dibuat</th>
								<th class="footer">Update Terakhir</th>
								<th class="footer"></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>