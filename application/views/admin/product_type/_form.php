<div class="item form-group group-product_type_name">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Produk</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($product_type)) echo $product_type['product_type_name'] ?>"
			class="form-control col-md-7 col-xs-12" name="product_type_name" placeholder="ex. Makanan" required="required"
			type="text">
		<div id="response-product_type_name" class="response-error"></div>
	</div>
</div>


<div class="ln_solid"></div>
<div class="form-group">
	<div class="col-md-6 col-md-offset-3">
		<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
	</div>
</div>