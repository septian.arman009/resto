<div class="page-title">
	<div class="title_left">
		<h3>Jenis Produk <small> Edit Data </small></h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Jenis Produk</h2>
					<div class="clearfix"></div>
				</div>
				
				<a href="<?= base_url('product_type') ?>" class="btn btn-default">Kembali</a>

				<div class="ln_solid"></div>
				
				<div class="x_content">

					<form class="form-horizontal form-label-left form-data" novalidate="" 
							action="<?= base_url('product_type/update/'. $product_type['product_type_id']) ?>"
							method="post"
							enctype="multipart/form-data"
							data-rule="default_update"
                            data-redirect="<?= base_url('product_type') ?>"
							data-btn="#btn-save">

						<?php $this->load->view('admin/product_type/_form') ?>

					</form>

				</div>
			</div>
		</div>
	</div>
</div>