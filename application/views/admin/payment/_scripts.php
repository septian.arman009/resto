<!-- validator -->
<script src="<?= base_url() ?>assets/admin/vendors/validator/validator.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>

<script>
	function detailOrder(tableId) {
		loadView("<?= base_url() ?>payment/detail_order/" + tableId, "#detail-order");
	}

	function custom_response($form, rule, message) {
        if(rule == 'custom_pay'){
            $("#meja-"+message).remove();
            notify('Sukses', 'Berhasil melakukan pembayaran', 'success');
            let orderId = $("#order_id").val();
		    loadView("<?= base_url() ?>payment/detail_order_id/" + orderId, "#detail-order");
        }
	}
</script>