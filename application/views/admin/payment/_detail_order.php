<script src="<?= base_url() ?>assets/admin/js/jquery.maskMoney.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>

<div class="col-md-3 widget widget_tally_box">
	<div class="x_panel fixed_height_420">
		<div class="x_content">
			<?php
                if($order->total_order <= 500000){
                    $fa = 'fa-star-o';
                }else if($order->total_order > 500000 && $order->total_order < 1000000){
                    $fa = 'fa-star-half-o';
                }else{
                    $fa = 'fa-star';
                }
            ?>
			<div class="flex">
				<ul class="list-inline widget_profile_box">
					<li>
						<a>
							<i class="fa <?= $fa ?>"></i>
						</a>
					</li>
					<li>
						<img src="<?= base_url('assets/admin/images/no_image.jpg') ?>" alt="..."
							class="img-circle profile_img">
					</li>
					<li>
						<a>
							<i class="fa <?= $fa  ?>"></i>
						</a>
					</li>
				</ul>
			</div>

			<h4 class="name"><?=  $order->customer_name ?></h4>

			<div class="flex">
				<ul class="list-inline count2">
					<li>
						<h3><?= str_replace('Meja ', '', $order->table_name) ?></h3>
						<span>Meja</span>
					</li>
					<li>
						<h3></h3>
						<span></span>
					</li>
					<li>
						<h3 class="pull-right"><?= str_replace('Rp. ', '', toRp($order->total_order)) ?></h3>
						<span>Tagihan</span>
					</li>
				</ul>
			</div>
			<p>
				<table>
					<tr>
						<td style="padding: 10px">Tanggal Pemesanan</td>
						<td style="padding: 10px">:</td>
						<td style="padding: 10px"><?= $order->created_at ?></td>
					</tr>
				</table>
			</p>
            <?php if($order->money_back <= 0){ ?>
			<form class="form-horizontal form-label-left form-data" novalidate=""
				action="<?= base_url('payment/pay') ?>" method="post" enctype="multipart/form-data" data-rule="custom_pay"
				data-btn="#btn-save" data-btn_response="Bayar">

                <input class="form-control" id="order_id" name="order_id" value="<?= $order->order_id ?>" style="display: none">
                <input class="form-control" name="total_order" value="<?= $order->total_order ?>" style="display: none">

				<div class="item form-group group-pay">
					<div class="col-md-12">
						<input class="form-control" value="Rp. 0" name="pay" id="pay" type="text">
						<div id="response-pay" class="response-error"></div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-3">
						<button id="btn-save" type="submit" class="btn btn-success">Bayar</button>
					</div>
				</div>

			</form>
            <?php } else {?>
					<div class="col-md-6 col-md-offset-3">
						<a onclick="kwitansi('<?= base_url('order/kwitansi/'.$order->order_id)?>', 'Kwitansi',480,600)" class="btn btn-default"><i class="fa fa-money"> Kwitansi</i></a>
					</div>
            <?php } ?>
		</div>
	</div>
</div>

<div class="col-md-9">
	<table class="table">
		<thead>
			<tr>
				<th>Nama Produk</th>
				<th>Harga</th>
				<th>Jumlah</th>
				<th>Total Harga</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($details as $detail) { ?>
			<tr>
				<td><?= $detail->product_name ?></td>
				<td><?= toRp($detail->order_price) ?></td>
				<td>x <?= $detail->qty ?></td>
				<td><?= toRp($detail->sub_total) ?></td>
			</tr>
			<?php } ?>
			<tr>
				<td></td>
				<td></td>
				<td><b>Total Tagihan</b></td>
				<td><b><?= toRp($order->total_order) ?></b></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><b>Bayar</b></td>
				<td><b><?= toRp($order->pay) ?></b></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><b>Kembalian</b></td>
				<td><b><?= toRp($order->money_back) ?></b></td>
			</tr>
		</tbody>
	</table>
</div>

<script>
	$(document).ready(function () {
		$('#pay').maskMoney({
			prefix: 'Rp. ',
			thousands: '.',
			decimal: ',',
			precision: 0
		});
	});

	function kwitansi(pageURL, title,w,h) {
        var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/2);
        var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    }
</script>