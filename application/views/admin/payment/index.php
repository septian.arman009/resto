<div class="page-title">
	<div class="title_left">
		<h3>Pembayaran</h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Pembayaran</h2>
					<div class="clearfix"></div>
				</div>
				<?php flash() ?>
				<div class="x_content">
					<form class="form-horizontal form-label-left">

						<div class="item form-group group-product_name">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Meja</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<select id="table_id" class="form-control" onchange="detailOrder($(this).val())">
									<option value="">- Pilih Nomor Meja -</option>
									<?php foreach ($tables as $table) { ?>
									<option id="meja-<?= $table->order_id ?>" value="<?= $table->table_id ?>"><?= $table->table_name ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

					</form>

					<div class="ln_solid"></div>

					<div class="row">

						<div id="detail-order">
							<div class="col-md-3 widget widget_tally_box">
								<div class="x_panel fixed_height_390">
									<div class="x_content">

										<div class="flex">
											<ul class="list-inline widget_profile_box">
												<li>
													<a>
														<i class="fa fa-spinner"></i>
													</a>
												</li>
												<li>
													<img src="<?= base_url('assets/admin/images/no_image.jpg') ?>"
														alt="..." class="img-circle profile_img">
												</li>
												<li>
													<a>
														<i class="fa fa-spinner"></i>
													</a>
												</li>
											</ul>
										</div>

										<h4 class="name">Nama Customer...</h4>

										<div class="flex">
											<ul class="list-inline count2">
												<li>
													<h3>...</h3>
													<span>Meja</span>
												</li>
												<li>
													<h3></h3>
													<span></span>
												</li>
												<li>
													<h3>...</h3>
													<span>Tagihan</span>
												</li>
											</ul>
										</div>
										<p>
											<table>
												<tr>
													<td style="padding: 10px">Tanggal Pemesanan</td>
													<td style="padding: 10px">:</td>
													<td style="padding: 10px">31/12/2019 00:00:00</td>
												</tr>
											</table>
										</p>
									</div>
								</div>
							</div>

							<div class="col-md-9">
								<table class="table">
									<thead>
										<tr>
											<th>Nama Produk</th>
											<th>Harga</th>
											<th>Jumlah</th>
											<th>Total Harga</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>
</div>