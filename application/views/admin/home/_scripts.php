<script src="<?= base_url() ?>assets/admin/vendors/raphael/raphael.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendors/morris.js/morris.min.js"></script>
<script>
    $(document).ready(function () {

        Morris.Bar({
            element: 'income-day',
            data: <?= json_encode($income_day) ?>,
            xkey: ['day'],
            ykeys: ['income_day'],
            labels: ['Total Penjualan'],
            barRatio: 0.2,
            barColors: ['#26B99A'],
            xLabelAngle: 50,
            hideHover: 'auto',
            resize: true
        });
        
    });
    
</script>