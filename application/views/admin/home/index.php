<div class="page-title">
	<div class="title_left">
		<h3>Beranda</h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">

			<div class="x_content">

				<div class="row tile_count">
					<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
						<span class="count_top"><i class="fa fa-users"></i> Total Pengunjung</span>
						<div class="count"><?= $total_customer_perday ?></div>
						<span class="count_bottom">Tanggal : <i class="green"><?= date('d/m/Y') ?></i> </span>
					</div>
					<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
						<span class="count_top"><i class="fa fa-users"></i> Total Pengunjung</span>
						<div class="count"><?= $total_customer_permonth ?></div>
						<span class="count_bottom">Bulan : <i class="green"><?= toIndoMonth(date('m')) ?></i> </span>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
						<span class="count_top"><i class="fa fa-money"></i> Pendapatan</span>
						<div class="count"><?= ($total_order_perday > 0 ) ? toRp($total_order_perday) : toRp(0) ?></div>
						<span class="count_bottom">Tanggal : <i class="green"><?= date('d/m/Y') ?></i> </span>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
						<span class="count_top"><i class="fa fa-money"></i> Pendapatan</span>
						<div class="count"><?= ($total_order_permonth > 0) ? toRp($total_order_permonth) : toRp(0) ?></div>
						<span class="count_bottom">Bulan : <i class="green"><?= toIndoMonth(date('m')) ?></i> </span>
					</div>
				</div>

				<div class="row">
					<div class="col-md-9">
						<div id="income-day"></div>
					</div>
					<div class="col-md-3">
						<div class="x_title">
							<h2>Top Menu</h2>
							<div class="clearfix"></div>
						</div>

						<div class="col-md-12">
						<?php foreach ($products as $product) { ?>
							<div>
								<p><?= $product->product_name.' ('.round($product->percen) ?> %)</p>
								<div class="">
									<div class="progress progress_sm" style="width: 100%;">
										<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="<?= $product->percen ?>"
											aria-valuenow="<?= $product->percen ?>" style="width: <?= $product->percen ?>%;"></div>
									</div>
								</div>
							</div>
						<?php } ?>
						</div>

					</div>
				</div>

				<div class="row tile_count">
					<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
						
					</div>
				</div>

			</div>
		</div>
	</div>
</div>