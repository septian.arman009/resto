<div class="page-title">
	<div class="title_left">
		<h3>Pesanan <small> (Order) </small></h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Pesanan</h2>
					<div class="clearfix"></div>
				</div>

				<div class="x_content">
					<div class="row">
						<div class="col-md-6">
							<div id="detail-order"></div>
						</div>

						<div class="col-md-6">
							<?php flash() ?>
							<input type="number" value="<?= $order->order_id ?>" id="order_id" style="display: none">
							<div class="animated flipInY">
								<div class="tile-stats">
									<div class="icon"><i class="fa fa-cutlery"></i>
									</div>
									<div class="count"><?= $order->customer_name ?></div>

									<h3><?= $order->table_name ?></h3>
									<p style="font-size: 20px" id="total-order">Total Pesanan : <?= toRp($order->total_order) ?></p>
								</div>

								<form class="form-horizontal form-label-left" novalidate="" 
									action="<?= base_url('order_list/update_status/'. $order->order_id.'/edit') ?>"
									method="post"
									enctype="multipart/form-data"
									data-rule="default_update"
									data-redirect="<?= base_url('order_list/edit/'.$order->order_id) ?>"
									data-btn="#btn-save">

									<div class="item form-group group-product_type_name">
										<div class="col-md-8 col-sm-8 col-xs-12">
											<select name="order_status" class="form-control">
												<option value="Pesanan diterima">- Status Pemesanan -</option>
											<?php 

												$orderStatus = array('Pesanan diterima', 'Sedang diproses', 'Pesanan diantar'); 
												foreach ($orderStatus as $key => $status) {
													$selected = ($status == $order->order_status) ? 'selected' : '';
													echo "<option $selected value='$status'>$status</option>";
												}
											?>
											</select>
										</div>

										<button type="submit" class="btn btn-primary" id='btn-save'>Simpan</button>
										<a class="btn btn-default" href="<?= base_url('order_list') ?>">Kembali</a>
									</div>

								</form>

							</div>
						</div>
					</div>

					<div class="ln_solid"></div>

					<div class="row">
						<div class="col-md-6">
							<h4>Kategori</h4>
							<div class="ln_solid"></div>

							<ul class="list-unstyled timeline">

								<?php foreach ($types as $type) { ?>
								<li>
									<div class="block">
										<div class="tags">
											<a class="tag">
												<span><?= $type->product_type_name ?></span>
											</a>
										</div>

										<?php foreach ($categories as $category) { 
												if($type->product_type_id == $category->product_type_id){?>

										<div class="block_content">
											<h2 class="title">
												<a id="category-<?= $category->category_id ?>" onclick="searchMenu('<?= $category->category_id ?>')"><?= $category->category_name ?></a>
											</h2>
											<div class="byline">
												<span><?= $category->total_product ?> Menu</span> by <a>D'Kampoeng</a>
											</div>
										</div>

										<?php } } ?>

									</div>
								</li>
								<?php } ?>

							</ul>
						</div>
						<div class="col-md-6">
							<h4>Daftar Menu</h4>
							<div class="ln_solid"></div>
							<ul class="list-unstyled msg_list">
								<div id="menu-list">
									<li>
										<a>
											<span class="image">
												<img style="width: 100px" src="<?= base_url('assets/admin/images/no_image.jpg') ?>" alt="img">
											</span>
											<span>
												<span style="font-size: 18px">ex. Gurame...</span>
												<span style="font-size: 18px"
													class="time">Rp. 15.0...</span>
											</span>
											<span class="message">
												1 Gurame, Sambal...
											</span>
										</a>
									</li>
								</div>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>