<div class="row">
    <div class="col-md-12">

        <form class="form-horizontal form-label-left" novalidate="" 
            action="<?= base_url('order_list/update_status/'. $order->order_id.'/modal') ?>"
            method="post"
            enctype="multipart/form-data">

            <div class="modal-body">

                <div class="item form-group group-product_type_name">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Status Pemesanan</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select name="order_status" class="form-control">
                        <?php 

                            $orderStatus = array('Pesanan diterima', 'Sedang diproses', 'Pesanan diantar'); 
                            foreach ($orderStatus as $key => $status) {
                                $selected = ($status == $order->order_status) ? 'selected' : '';
                                echo "<option $selected value='$status'>$status</option>";
                            }
                        ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id='btn-save'>Simpan</button>
            </div>
        </form>

    </div>
</div>