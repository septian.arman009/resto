<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?= $title ?></title>

	<link rel="icon" href="<?= base_url() ?>assets/admin/images/no_image.jpg" type="image/x-icon" />

	<!-- Bootstrap -->
	<link href="<?= base_url() ?>assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?= base_url() ?>assets/admin/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?= base_url() ?>assets/admin/vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- jQuery custom content scroller -->
	<link href="<?= base_url() ?>assets/admin/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css"
		rel="stylesheet" />
	<!-- PNotify -->
	<link href="<?= base_url() ?>assets/admin/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/admin/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/admin/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
	<!-- Custom Theme Style -->
	<link href="<?= base_url() ?>assets/admin/build/css/custom.min.css" rel="stylesheet">
	<!-- Sweetalert2 -->
	<link href="<?= base_url() ?>assets/admin/vendors/sweetalert/sweetalert.css" rel="stylesheet">
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col menu_fixed">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a href="index.html" class="site_title"><i class="fa fa-cutlery"></i> <span>D'Kampoeng</span></a>
					</div>

					<div class="clearfix"></div>

					<!-- menu profile quick info -->
					<div class="profile clearfix">
						<div class="profile_pic">
							<?php if(is_null(me()['photo'])) {?>
							<img src="<?= base_url() ?>assets/admin/images/img.jpg" alt="..."
								class="img-circle profile_img">
							<?php }else{ ?>
							<img src="<?= base_url() ?>assets/admin/photos/<?= me()['photo'] ?>" alt="..."
								class="img-circle profile_img">
							<?php } ?>
						</div>
						<div class="profile_info">
							<span>Selamat Datang</span>
							<h2><?php echo me()['name'] ?></h2>
						</div>
					</div>
					<!-- /menu profile quick info -->

					<br />

					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<h3>Menu Utama</h3>
							<ul class="nav side-menu">

								<?php menu('home', $menu, 'fa fa-home', 'Beranda') ?>

								<li <?php if($menu == "master") echo 'class="active"' ?>><a><i class="fa fa-hdd-o"></i> Master 
									<span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu" <?php if($menu == "master") echo 'style="display: block;"' ?>>

										<li <?php if($sub == 'user') echo 'class="active"'?>>
											<a href="<?= base_url('user') ?>">User</a>
										</li>

										<li <?php if($sub == 'product_type') echo 'class="active"'?>>
											<a href="<?= base_url('product_type') ?>">Jenis Produk</a>
										</li>

										<li <?php if($sub == 'category') echo 'class="active"'?>>
											<a href="<?= base_url('category') ?>">Kategori</a>
										</li>

										<li <?php if($sub == 'product') echo 'class="active"'?>>
											<a href="<?= base_url('product') ?>">Produk</a>
										</li>

										<li <?php if($sub == 'table') echo 'class="active"'?>>
											<a href="<?= base_url('table') ?>">Nomor Meja</a>
										</li>

									</ul>
								</li>

								<?php menu('order', $menu, 'fa fa-cutlery', 'Pemesanan') ?>
								<?php menu('payment', $menu, 'fa fa-money', 'Pembayaran') ?>

								<li <?php if($menu == "transaction") echo 'class="active"' ?>><a><i class="fa fa-exchange"></i> Transaksi 
									<span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu" <?php if($menu == "transaction") echo 'style="display: block;"' ?>>

										<li <?php if($sub == 'order_list') echo 'class="active"'?>>
											<a href="<?= base_url('order_list') ?>">Pesanan Berlangsung</a>
										</li>

										<li <?php if($sub == 'order_complete') echo 'class="active"'?>>
											<a href="<?= base_url('order_complete') ?>">Pesanan Selesai</a>
										</li>

										<li <?php if($sub == 'revision') echo 'class="active"'?>>
											<a href="<?= base_url('revision') ?>">Revisi Pesanan</a>
										</li>

									</ul>
								</li>

								<li <?php if($menu == "report") echo 'class="active"' ?>><a><i class="fa fa-bar-chart"></i> Laporan 
									<span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu" <?php if($menu == "report") echo 'style="display: block;"' ?>>

										<li <?php if($sub == 'statistic') echo 'class="active"'?>>
											<a href="<?= base_url('statistic/'.date('m').'/'.date('Y')) ?>">Statistik Penjualan</a>
										</li>

									</ul>
								</li>

							</ul>
						</div>

					</div>
					<!-- /sidebar menu -->

					<!-- /menu footer buttons -->
					<!-- <div class="sidebar-footer hidden-small">
						<a data-toggle="tooltip" data-placement="top" title="Settings">
							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="FullScreen">
							<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Lock">
							<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
							<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
						</a>
					</div> -->
					<!-- /menu footer buttons -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">
							<li class="">
								<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
									<?php if(is_null(me()['photo'])) {?>
									<img src="<?= base_url() ?>assets/admin/images/img.jpg" alt=""><?= me()['name'] ?>
									<?php }else{?>
									<img src="<?= base_url() ?>assets/admin/photos/<?= me()['photo'] ?>"
										alt=""><?= me()['name'] ?>
									<?php } ?>
									<span class=" fa fa-angle-down"></span>
								</a>
								<ul class="dropdown-menu dropdown-usermenu pull-right">
									<li><a href="<?= base_url('change_password')?>"> Ganti Password</a></li>
									<li><a class="logout"><i class="fa fa-sign-out pull-right"></i> Keluar</a></li>
								</ul>
							</li>

							<!-- <li role="presentation" class="dropdown">
								<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
									aria-expanded="false">
									<i class="fa fa-envelope-o"></i>
									<span class="badge bg-green">6</span>
								</a>
								<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
									<li>
										<a>
											<span class="image"><img src="<?= base_url() ?>assets/admin/images/img.jpg"
													alt="Profile Image" /></span>
											<span>
												<span>John Smith</span>
												<span class="time">3 mins ago</span>
											</span>
											<span class="message">
												Film festivals used to be do-or-die moments for movie makers. They were
												where...
											</span>
										</a>
									</li>
								</ul>
							</li> -->

						</ul>
					</nav>
				</div>
			</div>
			<!-- /top navigation -->

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
				<?php if(!empty($view)) $this->load->view($view) ?>
				</div>
			</div>
			<!-- /page content -->

			<!-- footer content -->
			<footer>
				<div class="pull-right">
					<a href="https://yaperi.com">D'Kampoeng</a> - 2019
				</div>
				<div class="clearfix"></div>
			</footer>
			<!-- /footer content -->
			<?php if(!empty($_modal)) $this->load->view($_modal) ?>
		</div>
	</div>

	<!-- jQuery -->
	<script src="<?= base_url() ?>assets/admin/vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?= base_url() ?>assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="<?= base_url() ?>assets/admin/vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script src="<?= base_url() ?>assets/admin/vendors/nprogress/nprogress.js"></script>
	<!-- jQuery custom content scroller -->
	<script
		src="<?= base_url() ?>assets/admin/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js">
	</script>
	<!-- PNotify -->
	<script src="<?= base_url() ?>assets/admin/vendors/pnotify/dist/pnotify.js"></script>
	<script src="<?= base_url() ?>assets/admin/vendors/pnotify/dist/pnotify.buttons.js"></script>
	<script src="<?= base_url() ?>assets/admin/vendors/pnotify/dist/pnotify.nonblock.js"></script>
	<!-- Datatables -->
	<script src="<?= base_url() ?>assets/admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?= base_url() ?>assets/admin/vendors/sweetalert/sweetalert.min.js"></script>
	<script src="<?= base_url() ?>assets/admin/js/ajax.js"></script>

	<?php if(!empty($scripts)) $this->load->view($scripts) ?>

	<script>
		$(document.body).on('click', '.logout', function (e) {
			swal({
					title: "Konfirmasi",
					text: "Apakah anda yakin ingin keluar ?",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Ya, Keluar",
					closeOnConfirm: false
				},
				function () {
					let url = "<?= base_url('logout') ?>";
					let type = 'get';
					postJson(url, type, data = {}, function (err, response) {
						if (response) {
							window.location.href = "<?= base_url('login') ?>";
						} else {
							console.log('ini error : ', err);
						}
					});
				});

		});
	</script>

	<!-- Custom Theme Scripts -->
	<script src="<?= base_url() ?>assets/admin/build/js/custom.js"></script>

	<style>
		.response-error {
			color: red;
		}

		.no-sort::after {
			display: none !important;
		}

		.no-sort {
			pointer-events: none !important;
			cursor: default !important;
		}

		a { 
			cursor: pointer;
		}

	</style>

	<?php if(!empty($styles)) $this->load->view($styles) ?>

</body>

</html>