<script src="<?= base_url() ?>assets/admin/vendors/validator/validator.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>

<form class="form-horizontal form-label-left form-data" 
    action="<?= base_url('order/store_order') ?>" 
    method="post"
	enctype="multipart/form-data" 
    data-rule="custom_store" 
    data-btn="#btn-save">

	<div class="modal-body">

		<div class="item form-group group-customer_name">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<input placeholder="Nama Customer" type="text" class="form-control" name="customer_name">
				<span id="response-customer_name" class="response-error"></span>
			</div>
		</div>

        <div class="item form-group group-table_id">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<select name="table_id" class="form-control">
                    <option value="">- Pilih Meja -</option>
                    <?php foreach ($tables as $table) { ?>
                        <option value="<?= $table->table_id ?>"><?= $table->table_name ?></option>
                    <?php } ?>
                </select>
				<span id="response-table_id" class="response-error"></span>
			</div>
		</div>

	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary" id='btn-save'>Simpan</button>
	</div>

</form>