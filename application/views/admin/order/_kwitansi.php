<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?= $title ?></title>

	<!-- Bootstrap -->
	<link href="<?= base_url() ?>assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom -->
</head>

<body class="nav-md">
	<div class="container body">
    <div class="text-center">
        <h3>D'Kampoeng</h3>
        <h4>Jl. Jalan Kiara Endah No.23, Bojong Kulur, Gunung Putri, Bogor,Jawa Barat.</h4>

    </div>
   
    <hr>
    <table>
        <tr>
            <td id="td"><strong>Nama Customer</strong></td>
            <td id="td">:</td>
            <td id="td"><?= $order->customer_name ?></td>
            <td id="td"></td>
            <td id="td"></td>
            <td id="td"></td>
            <td id="td"><strong>Tanggal Pesan</strong></td>
            <td id="td">:</td>
            <td id="td"><?= toIndoDatetime($order->created_at) ?></td>

        </tr>
        <tr>
            <td id="td"><strong>Nomor Meja</strong></td>
            <td id="td">:</td>
            <td id="td"><?= $order->table_name ?></td>
        </tr>
    </table>
    <table class="table table-bordered table-hover table-striped">
        <thead>
            <tr>
                <th>Nama Produk</th>
                <th>Harga</th>
                <th>Jumlah</th>
                <th>Sub Total</th>
            </tr>
        </thead>

        <tbody>
            <?php $total = 0; foreach ($details as $detail) { ?>
            <tr>    
                <td><?= $detail->product_name ?></td>
                <td><?= toRp($detail->order_price) ?></td>
                <td><?= $detail->qty ?> x</td>
                <td><?= toRp($detail->sub_total) ?></td>
            </tr>
            <?php $total += $detail->sub_total; } ?>
            <tr>
                <td></td>
                <td></td>
                <td><strong>Total</strong></td>
                <td><?= toRp($total) ?></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><strong>Bayar</strong></td>
                <td><?= toRp($order->pay) ?></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><strong>Kembalian</strong></td>
                <td><?= toRp($order->money_back) ?></td>
            </tr>
        </tbody>
    </table>
    <a onclick="printThis()">Print</a>
    </div>

	<!-- Bootstrap -->
	<script src="<?= base_url() ?>assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

</body>

<style>
    #td {
        padding: 5px;
    }
    a {
        cursor: pointer;
    }

    @media print
    {    
        a
        {
            display: none !important;
        }
    }
</style>

<script>
    function printThis() {
        window.print();
    }
</script>

</html>