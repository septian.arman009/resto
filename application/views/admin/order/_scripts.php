<!-- validator -->
<script src="<?= base_url() ?>assets/admin/vendors/validator/validator.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>

<script>
	$(document).ready(function () {

        if (document.body.contains(document.getElementById('order-table'))) {
			$('#order-table tfoot th').each(function () {
				var title = $(this).text();
				if (title != '')
					var inp = '<input type="text" class="form-control footer-s" placeholder="' + title +
						'" id="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#order-table').DataTable({
				"processing": false,
				"serverSide": true,
				"order": [
					[0, 'desc']
				],
				"ajax": {
					"url": '<?php if(!empty($table_url)) echo $table_url; ?>',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});

            if (document.body.contains(document.getElementById('order_id'))) {
                let orderId = $("#order_id").val();

                if(orderId != ""){
                detailOrder();
                }
            }
            
		}else{
            detailOrder();
        }

	});

    function searchMenu(categoryId) {
        loadView("<?= base_url() ?>order/search_menu_category/"+categoryId, "#menu-list");
        $(".cat-active").removeClass('cat-active');
        $("#category-"+categoryId).addClass('cat-active');
    }

    function addOrder() {
        $("#md-modal").modal('show');
        $(".modal-title").html("Pesanan Baru");
		let url = "<?= base_url() ?>order/new_order";
		loadView(url, '.modal-custom');
    }

    function addMenu(productId) {
        let orderId = $("#order_id").val();
        if(orderId != ""){
            doSomething("<?= base_url() ?>order/add_menu/"+orderId+'/'+productId);
            setTimeout(() => {
                detailOrder();
            }, 500);
        }else{
            notify('Terjadi Kesalahan', 'Data customer masih kosong', 'warning');
        }
    }

    function removeMenu(productId) {
        let orderId = $("#order_id").val();
        if(orderId != ""){
            doSomething("<?= base_url() ?>order/remove_menu/"+orderId+'/'+productId);
            setTimeout(() => {
                detailOrder();
            }, 500);
        }else{
            notify('Terjadi Kesalahan', 'Data customer masih kosong', 'warning');
        }
    }

    function detailOrder() {
        let orderId = $("#order_id").val();
        if(orderId != ""){
            loadView("<?= base_url() ?>order/detail_order/"+orderId, "#detail-order");
        }
        
    }

	function custom_response($form, rule, message) {
        if(rule == 'custom_store'){
            location.reload();
        }
    }
    
    function kwitansi(pageURL) {
        var left = (screen.width/2)-(480/2);
        var top = (screen.height/2)-(600/2);
        var targetWin = window.open (pageURL, 'Kwitansi', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+480+', height='+600+', top='+top+', left='+left);
    }

    function changeStatus(orderId) {
        $("#md-modal").modal('show');
        $(".modal-title").html("Ganti Status Pesanan");
		let url = "<?= base_url() ?>order_list/change_status/"+orderId;
		loadView(url, '.modal-custom');
    }
</script>

<style>
	#categories, #order-list {
		border: 1px solid #ccc;
    	border-radius: 5px;
	}

    .cat-active {
        font-size: 24px;
        color: #1ABB9C;
    }

    a:focus, a:hover {
        color: #1ABB9C;
    }

    .flipInY {
        cursor: pointer;
    }

    #order-table_filter {
		display: none;
	}
</style>