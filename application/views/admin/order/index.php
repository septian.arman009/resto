<div class="page-title">
	<div class="title_left">
		<h3>Pemesanan <small> (Order) </small></h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Pemesanan</h2>
					<div class="clearfix"></div>
				</div>

				<div class="x_content">
					<div class="row">
						<div class="col-md-6">
							<div id="detail-order"></div>
						</div>

						<div class="col-md-6">
							<?php flash() ?>
							<input type="number" value="<?= (!is_null($order)) ? $order->order_id : '' ?>" id="order_id" style="display: none">
							<?php if(!is_null($order)){ ?>
							<div class="animated flipInY">
								<div class="tile-stats">
									<div class="icon"><i class="fa fa-cutlery"></i>
									</div>
									<div class="count"><?= $order->customer_name ?></div>

									<h3><?= $order->table_name ?></h3>
									<p style="font-size: 20px" id="total-order">Total Pesanan : <?= toRp($order->total_order) ?></p>
								</div>
								<div class="pull-right">
									<a class="btn btn-success form-delete" data-url="<?= base_url('order/change_status_order') ?>">Buat Pesanan</a>
									<a class="btn btn-danger form-delete" data-url="<?= base_url('order/cancel_order') ?>">Batalkan Pesanan</a>
								</div>
							</div>
							<?php } else { ?>
							<div class="animated flipInY" onclick="addOrder()">
								<div class="tile-stats">
									<div class="icon"><i class="fa fa-cutlery"></i>
									</div>
									<div class="count">Nama Customer...</div>

									<h3>Nomor Meja...</h3>
									<p style="font-size: 20px">Total Pesanan : RP. 150.0...</p>
								</div>
							</div>
							<?php } ?>

						</div>
					</div>

					<div class="ln_solid"></div>

					<div class="row">
						<div class="col-md-6">
							<h4>Kategori</h4>
							<div class="ln_solid"></div>

							<ul class="list-unstyled timeline">

								<?php foreach ($types as $type) { ?>
								<li>
									<div class="block">
										<div class="tags">
											<a class="tag">
												<span><?= $type->product_type_name ?></span>
											</a>
										</div>

										<?php foreach ($categories as $category) { 
												if($type->product_type_id == $category->product_type_id){?>

										<div class="block_content">
											<h2 class="title">
												<a id="category-<?= $category->category_id ?>" onclick="searchMenu('<?= $category->category_id ?>')"><?= $category->category_name ?></a>
											</h2>
											<div class="byline">
												<span><?= $category->total_product ?> Menu</span> by <a>D'Kampoeng</a>
											</div>
										</div>

										<?php } } ?>

									</div>
								</li>
								<?php } ?>

							</ul>
						</div>
						<div class="col-md-6">
							<h4>Daftar Menu</h4>
							<div class="ln_solid"></div>
							<ul class="list-unstyled msg_list">
								<div id="menu-list">
									<li>
										<a>
											<span class="image">
												<img style="width: 100px" src="<?= base_url('assets/admin/images/no_image.jpg') ?>" alt="img">
											</span>
											<span>
												<span style="font-size: 18px">ex. Gurame...</span>
												<span style="font-size: 18px"
													class="time">Rp. 15.0...</span>
											</span>
											<span class="message">
												1 Gurame, Sambal...
											</span>
										</a>
									</li>
								</div>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>