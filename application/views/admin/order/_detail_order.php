<table class="table">
<thead>
    <tr>
        <th>Nama Produk</th>
        <th>Harga</th>
        <th>Jumlah</th>
        <th>Sub Total</th>
        <th>Aksi</th>
    </tr>
</thead>
    <?php foreach ($details as $detail) { ?>
        <tr>
            <td><?= $detail->product_name ?></td>
            <td><?= toRp($detail->order_price) ?></td>
            <td>x <?= $detail->qty ?></td>
            <td><?= toRp($detail->sub_total) ?></td>
            <td>
                <a onclick="addMenu('<?= $detail->product_id ?>')" class="btn btn-info btn-xs">+</a>|
                <a onclick="removeMenu('<?= $detail->product_id ?>')" class="btn btn-danger btn-xs">-</a>
            </td>
        </tr>
    <?php } ?>
</table>