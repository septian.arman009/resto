<section class="login_content">
	<form class="form-horizontal form-auth"
		action="<?= base_url('login_check') ?>"
        method="post"
        enctype="multipart/form-data"
		data-rule="default_update"
        data-redirect="<?= base_url('home') ?>"
        data-btn="#login"
		data-r_btn = "Masuk">

		<h1>Login</h1>

		<?php flash() ?>

		<div class="alert alert-danger alert-dismissible fade in" role="alert" id="login-error">
			<div id="response-error"><ul></ul></div>
		</div>

		<div class="item form-group group-email">
			<input type="email" name="email" placeholder="Email" required="required" class="form-control">
		</div>

		<div class="item form-group group-password">
			<input type="password" name="password" placeholder="Password" required="required" class="form-control">
		</div>

		<div>
			<button type="submit" class="btn btn-default" id="login">Masuk</button>
		</div>

		<div class="separator">
			<a href="<?= base_url('forgot_password') ?>" class="pull-right">Lupa Password ?</a>
		</div>

	</form>
</section>