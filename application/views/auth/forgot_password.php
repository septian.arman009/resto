<section class="login_content">
	<form class="form-horizontal form-auth"
		action="<?= base_url('send_link') ?>"
        method="post"
        enctype="multipart/form-data"
		data-rule="default_update"
        data-redirect="<?= base_url('forgot_password') ?>"
        data-btn="#send_link"
        data-r_btn = "Kirim">

		<h1>Lupas Password</h1>

		<?php flash() ?>

		<div class="alert alert-danger alert-dismissible fade in" role="alert" id="login-error">
			<div id="response-error"><ul></ul></div>
		</div>

		<div class="item form-group group-email">
			<input type="email" name="email" placeholder="Email" required="required" class="form-control">
		</div>

		<div>
			<button type="submit" class="btn btn-default" id="send_link">Kirim</button>
		</div>

		<div class="separator">
			<a href="<?= base_url('login') ?>" class="pull-right">Login</a>
		</div>

	</form>
</section>