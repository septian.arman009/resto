-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2020 at 02:25 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_resto`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `fRupiah` (`number` BIGINT) RETURNS VARCHAR(255) CHARSET latin1 BEGIN
DECLARE hasil VARCHAR(255);
SET hasil = REPLACE(REPLACE(REPLACE(FORMAT(number, 0), '.', '|'), ',', '.'), '|', ',');
RETURN (hasil);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) NOT NULL,
  `category_name` varchar(30) NOT NULL,
  `product_type_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `product_type_id`, `created_at`, `updated_at`) VALUES
(1, 'Ikan Gurame', 1, '2020-01-04 13:12:04', '2020-01-04 13:12:04'),
(2, 'Ikan Lele', 1, '2020-01-04 13:12:04', '2020-01-04 13:12:04'),
(3, 'Jeruk', 2, '2020-01-04 13:12:04', '2020-01-04 13:12:04'),
(4, 'Teh', 2, '2020-01-04 13:12:04', '2020-01-04 13:12:04');

-- --------------------------------------------------------

--
-- Table structure for table `customer_orders`
--

CREATE TABLE `customer_orders` (
  `order_id` int(10) NOT NULL,
  `table_id` int(10) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `total_order` int(10) NOT NULL,
  `pay` int(10) NOT NULL,
  `money_back` int(10) NOT NULL,
  `money_back_revision` int(10) NOT NULL,
  `extra` int(10) NOT NULL,
  `status` int(1) NOT NULL,
  `revision` text,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_orders`
--

INSERT INTO `customer_orders` (`order_id`, `table_id`, `customer_name`, `total_order`, `pay`, `money_back`, `money_back_revision`, `extra`, `status`, `revision`, `created_at`) VALUES
(1, 1, 'Ariel', 412500, 415000, 2500, 2500, 0, 3, 'Kekurangan pembayaran pelanggan Rp. 7.500', '2020-01-04 13:14:39');

-- --------------------------------------------------------

--
-- Table structure for table `detail_orders`
--

CREATE TABLE `detail_orders` (
  `detail_id` int(10) NOT NULL,
  `order_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `qty` int(10) NOT NULL,
  `order_price` int(10) NOT NULL,
  `sub_total` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_orders`
--

INSERT INTO `detail_orders` (`detail_id`, `order_id`, `product_id`, `qty`, `order_price`, `sub_total`) VALUES
(1, 1, 1, 3, 85000, 255000),
(2, 1, 2, 2, 30000, 60000),
(3, 1, 3, 13, 7500, 97500);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(10) NOT NULL,
  `product_name` varchar(150) NOT NULL,
  `category_id` int(10) NOT NULL,
  `description` text NOT NULL,
  `price` int(10) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `category_id`, `description`, `price`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'Gurame Asam Manis', 1, '1 Iakn Gurame, Lalaban', 85000, '6b6f13e19187a1601b04c5922c45d10e.jpg', '2020-01-04 13:13:14', '2020-01-04 13:13:14'),
(2, 'Pecel Lele', 2, '1 Pecel Lele, Nasi, Lalab, 1 Tahu, 1 Tempe', 30000, 'd5054a41097b61776f1a8bd382be1812.jpg', '2020-01-04 13:13:41', '2020-01-04 13:13:41'),
(3, 'Es Jeruk', 3, '1 Gelas Es Jeruk 200ml', 7500, '5d05abbe50134c40b54c363c1786cf63.jpg', '2020-01-04 13:14:06', '2020-01-04 13:14:06');

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `product_type_id` int(10) NOT NULL,
  `product_type_name` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`product_type_id`, `product_type_name`, `created_at`, `updated_at`) VALUES
(1, 'Makanan', '2020-01-04 13:11:04', '2020-01-04 13:11:04'),
(2, 'Minuman', '2020-01-04 13:12:04', '2020-01-04 13:12:04'),
(3, 'Snack', '2020-01-04 13:12:04', '2020-01-04 13:12:04'),
(4, 'Paket', '2020-01-04 13:12:04', '2020-01-04 13:12:04');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `setting_id` int(1) NOT NULL,
  `protocol` varchar(30) NOT NULL,
  `mail_host` varchar(30) NOT NULL,
  `mail_port` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_id`, `protocol`, `mail_host`, `mail_port`) VALUES
(1, 'smtp', 'ssl://smtp.gmail.com', 465);

-- --------------------------------------------------------

--
-- Table structure for table `table_numbers`
--

CREATE TABLE `table_numbers` (
  `table_id` int(10) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_numbers`
--

INSERT INTO `table_numbers` (`table_id`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'Meja 1', '2020-01-04 13:14:19', '2020-01-04 13:14:19'),
(2, 'Meja 2', '2020-01-04 13:14:19', '2020-01-04 13:14:19'),
(3, 'Meja 3', '2020-01-04 13:14:20', '2020-01-04 13:14:20'),
(4, 'Meja 4', '2020-01-04 13:14:20', '2020-01-04 13:14:20'),
(5, 'Meja 5', '2020-01-04 13:14:21', '2020-01-04 13:14:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `verified` int(1) NOT NULL,
  `verification_token` varchar(255) DEFAULT NULL,
  `reset_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `photo`, `verified`, `verification_token`, `reset_token`, `created_at`, `updated_at`) VALUES
(4, 'Arman S', 'septian.arman009@gmail.com', '5456db00ec97635f7d998e3290a01b6d', '4da92dbfb9b77d59f9301e0ef6713b1e.jpg', 1, '', '79d602ae32e1bcd1af535a01564e3f317ab204b60257ab1c9369412f95676969', '2019-12-01 02:03:30', '2019-12-03 09:20:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `customer_orders`
--
ALTER TABLE `customer_orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `detail_orders`
--
ALTER TABLE `detail_orders`
  ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`product_type_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `table_numbers`
--
ALTER TABLE `table_numbers`
  ADD PRIMARY KEY (`table_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customer_orders`
--
ALTER TABLE `customer_orders`
  MODIFY `order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `detail_orders`
--
ALTER TABLE `detail_orders`
  MODIFY `detail_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `product_type_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `setting_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `table_numbers`
--
ALTER TABLE `table_numbers`
  MODIFY `table_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
