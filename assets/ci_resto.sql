-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2019 at 06:32 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_resto`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `fRupiah` (`number` BIGINT) RETURNS VARCHAR(255) CHARSET latin1 BEGIN
DECLARE hasil VARCHAR(255);
SET hasil = REPLACE(REPLACE(REPLACE(FORMAT(number, 0), '.', '|'), ',', '.'), '|', ',');
RETURN (hasil);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) NOT NULL,
  `category_name` varchar(30) NOT NULL,
  `product_type_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `product_type_id`, `created_at`, `updated_at`) VALUES
(9, 'Ikan Lele', 3, '2019-12-29 06:51:29', '2019-12-29 06:51:29'),
(10, 'Ikan Gurame', 3, '2019-12-29 06:51:29', '2019-12-29 06:51:29'),
(11, 'Ayam', 3, '2019-12-29 06:51:29', '2019-12-29 06:51:29'),
(12, 'Daging', 3, '2019-12-29 06:51:29', '2019-12-29 06:51:29'),
(13, 'Jeruk', 4, '2019-12-29 06:51:29', '2019-12-29 06:51:29'),
(14, 'Teh', 4, '2019-12-29 06:51:29', '2019-12-29 06:51:29');

-- --------------------------------------------------------

--
-- Table structure for table `customer_orders`
--

CREATE TABLE `customer_orders` (
  `order_id` int(10) NOT NULL,
  `table_id` int(10) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `total_order` int(10) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_orders`
--

INSERT INTO `customer_orders` (`order_id`, `table_id`, `customer_name`, `total_order`, `status`, `created_at`) VALUES
(8, 8, 'Arman Septian', 210000, 1, '2019-12-29 17:28:16');

-- --------------------------------------------------------

--
-- Table structure for table `detail_orders`
--

CREATE TABLE `detail_orders` (
  `detail_id` int(10) NOT NULL,
  `order_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `qty` int(10) NOT NULL,
  `order_price` int(10) NOT NULL,
  `sub_total` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_orders`
--

INSERT INTO `detail_orders` (`detail_id`, `order_id`, `product_id`, `qty`, `order_price`, `sub_total`) VALUES
(13, 4, 3, 1, 85000, 85000),
(14, 4, 4, 1, 25000, 25000),
(15, 4, 5, 1, 7500, 7500),
(16, 4, 6, 1, 5000, 5000),
(17, 4, 6, 1, 5000, 5000),
(18, 4, 3, 1, 85000, 85000),
(19, 4, 4, 1, 25000, 25000),
(20, 4, 3, 1, 85000, 85000),
(21, 4, 3, 1, 85000, 85000),
(22, 4, 3, 1, 85000, 85000),
(23, 4, 3, 1, 85000, 85000),
(24, 4, 5, 1, 7500, 7500),
(25, 4, 5, 1, 7500, 7500),
(26, 4, 5, 1, 7500, 7500),
(27, 5, 3, 1, 85000, 85000),
(28, 5, 5, 1, 7500, 7500),
(29, 5, 5, 1, 7500, 7500),
(30, 5, 6, 1, 5000, 5000),
(31, 5, 4, 1, 25000, 25000),
(32, 6, 3, 2, 85000, 680000),
(33, 7, 3, 3, 85000, 255000),
(35, 8, 4, 4, 25000, 100000),
(36, 8, 5, 2, 7500, 15000),
(37, 8, 6, 2, 5000, 10000),
(38, 8, 3, 1, 85000, 85000);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(10) NOT NULL,
  `product_name` varchar(150) NOT NULL,
  `category_id` int(10) NOT NULL,
  `description` text NOT NULL,
  `price` int(10) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `category_id`, `description`, `price`, `photo`, `created_at`, `updated_at`) VALUES
(3, 'Gurame Asam Mamis', 10, '1 gurame, sambal saus manis', 85000, 'b379985294c4a7d5655969e793b0406e.jpg', '2019-12-29 06:52:22', '2019-12-29 08:37:42'),
(4, 'Pecel Lele Lela', 9, '1 ikan lele, sambal, nasi', 25000, '18207fa73b56a49f19d7dbb4285a852e.jpg', '2019-12-29 06:53:17', '2019-12-29 08:43:43'),
(5, 'Es Jeruk Manis Dingin', 13, 'Es Jeruk', 7500, 'ac5bc4a9bb72736c6101b6c55912f8e5.jpg', '2019-12-29 09:29:02', '2019-12-29 09:29:02'),
(6, 'Es Teh Manis', 14, 'teh dengan es', 5000, 'a0071157a43bddd463c7668752e0b64e.png', '2019-12-29 09:30:30', '2019-12-29 09:30:30');

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `product_type_id` int(10) NOT NULL,
  `product_type_name` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`product_type_id`, `product_type_name`, `created_at`, `updated_at`) VALUES
(3, 'Makanan', '2019-12-27 05:50:27', '2019-12-27 05:50:27'),
(4, 'Minuman', '2019-12-27 05:50:27', '2019-12-27 05:50:27');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `setting_id` int(1) NOT NULL,
  `protocol` varchar(30) NOT NULL,
  `mail_host` varchar(30) NOT NULL,
  `mail_port` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_id`, `protocol`, `mail_host`, `mail_port`) VALUES
(1, 'smtp', 'ssl://smtp.gmail.com', 465);

-- --------------------------------------------------------

--
-- Table structure for table `table_numbers`
--

CREATE TABLE `table_numbers` (
  `table_id` int(10) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_numbers`
--

INSERT INTO `table_numbers` (`table_id`, `table_name`, `created_at`, `updated_at`) VALUES
(4, 'Meja 1', '2019-12-28 04:19:06', '2019-12-28 04:19:06'),
(5, 'Meja 2', '2019-12-28 04:20:33', '2019-12-28 04:20:33'),
(7, 'Meja 4', '2019-12-28 04:20:55', '2019-12-28 04:20:55'),
(8, 'Meja 10', '2019-12-29 09:32:37', '2019-12-29 09:32:37'),
(9, 'Meja 3', '2019-12-29 09:32:46', '2019-12-29 09:32:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `verified` int(1) NOT NULL,
  `verification_token` varchar(255) DEFAULT NULL,
  `reset_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `photo`, `verified`, `verification_token`, `reset_token`, `created_at`, `updated_at`) VALUES
(4, 'Arman S', 'septian.arman009@gmail.com', '5456db00ec97635f7d998e3290a01b6d', '4da92dbfb9b77d59f9301e0ef6713b1e.jpg', 1, '', NULL, '2019-12-01 02:03:30', '2019-12-03 09:20:50'),
(5, 'ariel', 'ariel@gmail.com', '5456db00ec97635f7d998e3290a01b6d', NULL, 1, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `customer_orders`
--
ALTER TABLE `customer_orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `detail_orders`
--
ALTER TABLE `detail_orders`
  ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`product_type_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `table_numbers`
--
ALTER TABLE `table_numbers`
  ADD PRIMARY KEY (`table_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `customer_orders`
--
ALTER TABLE `customer_orders`
  MODIFY `order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `detail_orders`
--
ALTER TABLE `detail_orders`
  MODIFY `detail_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `product_type_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `setting_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `table_numbers`
--
ALTER TABLE `table_numbers`
  MODIFY `table_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
