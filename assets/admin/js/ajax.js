function postFormData(url, method, data, callback) {
	$.ajax({
		url: url,
		type: method,
		data: data,
		processData: false,
		contentType: false,
		success: function (response) {
			return callback(null, response);
		},
		error: function (err) {
			return callback(true, err);
		}
	});
}

function postJson(url, method, data, callback) {
	$.ajax({
		url: url,
		type: method,
		dataType: 'json',
		data: data,
		success: function (response) {
			return callback(null, response);
		},
		error: function (err) {
			return callback(true, err);
		}
	});
}

function loadView(url, div) {
    $.ajax({
        url: url,
        success: function (data) {
            $(div).html(data);
        }
    });
}

function doSomething(url) {
    $.ajax({
        url: url,
        success: function (response) {
			if(response.rule == 'order'){
				$("#total-order").html(response.total_order);
			}else if(response.rule == 'revision'){
				$("#total-order").html("Total Pesanan <a class='pull-right' style='margin-right: 5px'>"+response.total_order+"</a>");
				$("#extra").html("Extra <a class='pull-right' style='margin-right: 5px'>"+response.extra+"</a>");
				if(response.extra_money > 0){
					$("#add-money-back").hide();
					$("#add-payment").show();
					$("#finish").hide();
				}else if(response.extra_money < 0){
					$("#add-payment").hide();
					$("#add-money-back").show();
					$("#finish").hide();
				}else if(response.extra_money == 0){
					$("#add-payment").hide();
					$("#add-money-back").hide();
					$("#finish").show();
				}
			}
        }
    });
}