if (typeof (validator) != 'undefined') {
	$('form')
		.on('blur', 'input[required], input.optional, select.required', validator.checkField)
		.on('change', 'select.required', validator.checkField)
		.on('keypress', 'input[required][pattern]', validator.keypress);

	$('.multi.required').on('keyup blur', 'input', function () {
		validator.checkField.apply($(this).siblings().last()[0]);
	});
}

$('.form-data').submit(function (e) {
	e.preventDefault();
	var submit = true;

	if (typeof (validator) != 'undefined') {
		if (!validator.checkAll($(this))) {
			submit = false;
		}
	}

	if (submit) {
		$(".response-error").html('');
		$(".has-error").removeClass('has-error');
		
		let $form = $(this);
		let data = new FormData(this);
		let method = $form.attr('method');
		let url = $form.attr('action');
		let rule = $form.data('rule');
		let btn = $form.data('btn');
		$(btn).html('Loading..');
		let btnResponse = $form.data('btn_response');
		
		postFormData(url, method, data, function (err, response) {
			if (response) {
				if ($.isEmptyObject(response.errors)) {
					document.activeElement.blur();
					if(btnResponse){
						$(btn).html(btnResponse);
					}else{
						$(btn).html('Simpan');
					}
					form_save_response($form, rule, response.message);
				} else {
					if(btnResponse){
						$(btn).html(btnResponse);
					}else{
						$(btn).html('Simpan');
					}
					$.each(response.errors, function (key, value) {	
						$(".group-" + key).addClass('has-error');
						$("#response-" + key).append('<span>' + value + '</span>');
					});
				}
			} else {
				console.log("Error : ", err)
			}
		});
	}

	return false;
});

function form_save_response($form, rule, message) {
	if (rule == "default_store") {
		$form.trigger("reset");
		notify('Sukses', message, 'success');
	} else if (rule == "default_update") {
		window.location = $form.data('redirect');
	} else {
		custom_response($form, rule, message);
	}
}

$(document.body).on('click', '.form-delete', function (e) {
	let $form = $(this);
	let message = $form.data('confirm') ? $form.data('confirm') : 'Anda yakin ingin melakukan hal ini ?';
	swal({
			title: "Konfirmasi",
			text: message,
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Ya",
			closeOnConfirm: false
		},
		function () {
			let url = $form.data('url');
			let type = 'delete';
			postJson(url, type, data = {}, function (err, response) {
				if (response) {
					if ($.isEmptyObject(response.errors)) {
						if($.isEmptyObject(response.rules)){
							
							if(response.id){
								swal.close();
								baris = $('#form-' + response.id).closest('tr');
								baris.fadeOut(200, function () {
									$form.remove();
								});
							}else{
								location.reload();
							}

						}else{
							swal.close();
							custom_response(null, response.rules.rule, response.rules.message);
						}
						
					} else {
						swal.close();
						notify('Terjadi Kesalahan', response.errors, 'error');
					}
				} else {
					console.log('Error : ', err);
				}
			});
		});

});

function notify(title, text, type) {
	new PNotify({
		title: title,
		text: text,
		type: type,
		styling: 'bootstrap3'
	});
}